clc;
clear all;
close all;

setup;

%-------------------------------------------------------------------------------------------------------------------------
simulation_output_directory         =('3D_N50016_L70.0_CCR0.85_PHI0.076_EC277.21_DC27.71_KMAX8_RANDQ1.00e+00_CP1.0000_BOTHRK_1_phi_0p076_K_1p0');
simulation_output_directory         =sprintf('/scratch.local/data/data-galemd-output/%s',simulation_output_directory);

% simulation_output_directory       =('/scratch.local/data/singh/dev-blackdog/cppbox/out');
% 
 simulation_output_directory        =('3D_N262144_L6.3_CCR0.85_PHI0.400_EC277.21_DC27.71_1_sph');
 simulation_output_directory        =sprintf('/scratch.local/data/data-galemd-output/%s',simulation_output_directory);


%|david-internship|-----
%simulation_output_directory        =('3D_N50016_L70.0_CCR0.85_PHI0.076_EC277.21_DC27.71_KMAX8_RANDQ3.00e-01_CP1.0000_BOTHRK_1');
%simulation_output_directory        =sprintf('/scratch.local/data/data-galemd-output-david/%s',simulation_output_directory);
%-----------------------


simulation_output_directory_neutral =('3D_N50016_L70.0_CCR0.85_PHI0.076_EC277.21_DC27.71_1');
simulation_output_directory_neutral =sprintf('/scratch.local/data/data-galemd-archive/2018-08-09-gaussian-charge-plus-charge-exchange-plus-neutral-plus-longtime/%s',simulation_output_directory_neutral);

global matlab_output_directory;
matlab_output_directory             =('/scratch.local/data/singh/dev/matbox/out');
%--------------------------------------------------------------------------------------------------------------------------
global CHARGE;
global CELL_LENGTH;
global FLOATTYPE;
global DIM3;
global VIEW_ANGLE;
global N_PARTICLE;
global ITER_DATA_GEN;
global DELTA_T;
global TIME_STEP_SKIP;
global PARTICLE_SKIP;
global T_HAFF_YSHIFT;
global TAU_INV;
global HAFFS_EXPONENT;
global DOT_SIZE;
%-----------------------------------------------------------------------------------------------------------
FLOAT_TYPE              = 1;
CHARGE                  = 1;
CELL_LENGTH             = 70;%2*pi;%33;%2*pi;%60;
if(FLOAT_TYPE==1)
    FLOATTYPE           = 'float';
else
    FLOATTYPE           = 'double';
end
DIM3                    = 1;
MOVIE                   = 0;
video_name              = sprintf('%s',matlab_output_directory);
N_PARTICLE              = 50016;%262144;%54872;%50016; %54872;%110592;%54872;%5000;%54872;%262144; %54872;%500;
ITER_DATA_GEN           = 1000;
DELTA_T                 = 0.001;%0.0001
TIME_STEP_SKIP          = 1;
PARTICLE_SKIP           = 1;
T_HAFF_YSHIFT           = 1000.0;
TAU_INV                 = 10.0;
V_RES                   = 1;
if(V_RES==1)
    HAFFS_EXPONENT      = -5/3;
else
    HAFFS_EXPONENT      = -2;
end

DIAMETER                = 1;
RADIUS                  = DIAMETER/2;
NUMBER_DENSITY          = N_PARTICLE/CELL_LENGTH^3;
VOLUME_FRACTION         = (N_PARTICLE/CELL_LENGTH^3)*(4/3)*(pi*RADIUS^3);
DOT_SIZE                = 2*10*(2.0 * ((3*0.001*CELL_LENGTH^2)/(4*N_PARTICLE*pi))^(1/3));
FONT_SIZE               = 14;
VIEW_ANGLE              = 00;
%-----------------------------------------------------------------------------------------------------------
VELOCITY_VECTORS        = 0;
MIDPLANE_VELOCITY_SPH   = 0;
MIDPLANE_DENSITY_SPH    = 0;
PARTICLES_IN_BOX        = 0;
PLOT_KE                 = 0;
PLOT_DISSIPATION_RATE   = 0;
PLOT_FLUCTUATIONS       = 0;
PLOT_TEMPERATURE        = 0;
SAVE_TEMPERATURE        = 0;
PLOT_V_DIST             = 0;
PLOT_F_DIST             = 0;
SAVE_V_DIST             = 0;
PLOT_VABS_DIST          = 0;
PLOT_FORCES             = 0;
PLOT_CHARGE_VS_TIME     = 0;
PLOT_CHARGE_DIST        = 0;
PLOT_COLLISIONS         = 0;
N_STICK                 = 0;
SINGLE_PARTICLE_IN_BOX  = 0;
PARTICLE_ID_TO_TRACK=[45755; 27454; 10000];
PLOT_MSD                = 0;

CLUSTER                 = 1; 
if CLUSTER
    clusterSwitches;
end


%|handles|----------------------------------------------------------------------------------------------------------------
%set(fig_18,'YTick',[-4 -2 0 2 4]);
[fig_0,fig_0_gcf]  = createFig(VELOCITY_VECTORS,         'empty',            'empty',    'holdoff',  'boxon','linlin',14);
%[fig_0,fig_0_gcf]  = createFig(SINGLE_PARTICLE_IN_BOX,   '',                 '',         'holdon',   '',     'linlin',14);
[fig_1,fig_1_gcf]  = createFig(PARTICLES_IN_BOX,         'empty',            'empty',    'holdoff',  'boxon','linlin',14);
[fig_2,fig_2_gcf]  = createFig(PLOT_FORCES,              'Particle index',   '$F_z$',    'holdon',   'boxon','linlin',14);
[fig_3,fig_3_gcf]  = createFig(PLOT_TEMPERATURE,         'time $t v_\textrm{ref}/d$',    'granular temperature $T$', 'holdon',   'boxon','loglog',14);
[fig_4,fig_4_gcf]      = createFig(PLOT_FLUCTUATIONS,    'time $t v_\textrm{ref}/d$',    '$\langle\delta O^2\rangle$', 'holdon',   'boxon','loglog',18);
%[fig_4_1,fig_4_1_gcf]  = createFig(PLOT_FLUCTUATIONS,    'Mach number $\mathcal{M}$',    '$\rho=\frac{\langle(n-n_h)^2\rangle^{1/2}}{n_h}$', 'holdon',   'boxon','loglog',14);
[fig_5,fig_5_gcf]  = createFig(PLOT_KE,                  '$t/\tau$',         '$E$',      'holdon',   'boxon','linlin',14);
[fig_7,fig_7_gcf]  = createFig(PLOT_DISSIPATION_RATE,    '$t/\tau$',         '$-\frac{dE}{dt}$',      'holdon',   'boxon','linlin',14);
[fig_6,fig_6_gcf]  = createFig(PLOT_CHARGE_VS_TIME,      '$t/\tau$',         '$Var(q)$',    'holdon',   'boxon','linlin',14);
[fig_8,fig_8_gcf]  = createFig(PLOT_F_DIST,              '$F_x/<F_x^2>^{1/2}$','$P(F_x)$','holdon',  'boxon','linlog',14);
[fig_9,fig_9_gcf]  = createFig(PLOT_V_DIST,              '$v_x/<v_x^2>^{1/2}$','$P(v_x)$','holdon',  'boxon','linlog',14);
[fig_10,fig_10_gcf]= createFig(PLOT_CHARGE_DIST,         '$q/<q^2>^{1/2}$',   '$f(q)$',  'holdon',   'boxon','linlog',14);
[fig_11,fig_11_gcf]= createFig(PLOT_VABS_DIST,           '$v/<v^2>^{1/2}$',   '$f(v)$',  'holdon',   'boxon','linlog',14);
[fig_12,fig_12_gcf]= createFig(PLOT_COLLISIONS,          '$t/\tau$','$N_\mathrm{{stick}}$','holdon', 'boxon','linlin',14);
[fig_16,fig_16_gcf]= createFig(PLOT_MSD,                 '$t/\tau$',    '$<\Delta \bf{r}^2>$','holdon',   'boxon','loglog',14);
[fig_36,fig_36_gcf]  = createFig(MIDPLANE_DENSITY_SPH,        'x',                          'y',             'holdoff',  'boxon',   'linlin',14);
[fig_37,fig_37_gcf]  = createFig(MIDPLANE_VELOCITY_SPH,        '$v_x$',                          '$z$',             'holdon',  'boxon',   'linlin',14);

GAMMA = 0;
[fig_50,fig_50_gcf]  = createFig(GAMMA,        '$t$',                          'O',             'holdoff',  'boxon',   'linlin',14);
%-------------------------------------------------------------------------------------------------------------------

if(MOVIE)
    video_name = VideoWriter(strcat('/scratch.local/data/singh/dev/matbox/',video_name,'.avi'));
    open(video_name);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
listing     = dir(strcat((simulation_output_directory),'/positionX_t*.dat'));
listingName = {listing(:).name};
listing     = natsortfiles(listingName);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
listing_neutral  = dir(strcat((simulation_output_directory_neutral),'/positionX_t*.dat'));
listingName      = {listing_neutral(:).name};
listing_neutral  = natsortfiles(listingName);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

base = 1.6;

END_STEP = floor(log(length(listing))/log(base));

if(base==1)
END_STEP = length(listing);
TIME_STEP_SKIP = TIME_STEP_SKIP;
end

for iter=1:TIME_STEP_SKIP:END_STEP
      
    pause(1e-16);
    
    step = floor(base^iter)+1;
    if(base==1)
       step = iter; 
    end
    
    timestep = (step-1)*ITER_DATA_GEN;
    time     = timestep*DELTA_T
    %|get data|------------------------------------------------------------
    foo = char(listing(1,step));
    timestring = (foo(strfind(foo,'_t')+2 :1: strfind(foo,'.dat')-1));
    %|get data|------------------------------------------------------------
    foo = char(listing_neutral(1,step));
    timestring_neutral = (foo(strfind(foo,'_t')+2 :1: strfind(foo,'.dat')-1));
    %----------------------------------------------------------------------   
    
    [energy,density,charge,positionX,positionY,positionZ, velocityX,velocityY,velocityZ,forceX,forceY,forceZ]...
    = getData(simulation_output_directory, timestring, FLOATTYPE,1,0,1,1);%1011 %density,charge,force,energy
    
    [energy_neutral,density_neutral,charge_neutral,positionX_neutral,positionY_neutral,positionZ_neutral, ...
        velocityX_neutral,velocityY_neutral,velocityZ_neutral,forceX_neutral,forceY_neutral,forceZ_neutral]...
    = getData(simulation_output_directory_neutral, timestring_neutral, FLOATTYPE,0,0,0,0);      
    
    if(VELOCITY_VECTORS)   
    %quiver3(fig_0,positionX,positionY,positionZ,velocityX,velocityY,velocityZ); 
    %set(fig_0,'View',[-VIEW_ANGLE,VIEW_ANGLE]);
    %axis(fig_0,[-CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2]);
    boxVelocity(fig_0,time,density,positionX,positionY,positionZ,velocityX,velocityY,velocityZ);
    end
    
    if(MIDPLANE_DENSITY_SPH)
        midPlaneDensity(fig_36,time,density,positionX,positionY,positionZ,velocityX,velocityY,velocityZ);
    end
    
    if(MIDPLANE_VELOCITY_SPH)
        midPlaneVelocity(fig_37,time,density,positionX,positionY,positionZ,velocityX,velocityY,velocityZ);
        set(fig_37,'xlim',[-0.5 0.5]); 
        set(fig_37,'ylim',[-CELL_LENGTH/2 CELL_LENGTH/2]); 
    end

    if(PARTICLES_IN_BOX)
        [s]=domain(fig_1,positionX,positionY,positionZ,'fixed_color',positionZ(1:PARTICLE_SKIP:end),'show_axis_lines');
        if(MOVIE)
            makeMovie(fig_1_gcf,video_name);
        end  
    end
   
    if(PLOT_TEMPERATURE)
        %[leg_3,Tg_haff,ke]= granularTemperature(fig_3,time,velocityX,velocityY,velocityZ);
        [leg_3,Tg_haff,Tg_sim] = granularTemperaturePrecise(fig_3,time,velocityX,velocityY,velocityZ,positionX,positionY,positionZ);                      
    end
    
    if(PLOT_FLUCTUATIONS)        
         [w]=fluctuations(fig_4,fig_4_gcf,time,velocityX,velocityY,velocityZ,positionX,positionY,positionZ,charge,1);
            set(fig_4,'xlim',[01 1200]); 
            Ma_simulation(iter) = w;           
            t_simulation(iter) = time;
            
         %[w]=fluctuations(fig_4,fig_4_gcf,time,velocityX_neutral,velocityY_neutral,velocityZ_neutral,...
          %                           positionX_neutral,positionY_neutral,positionZ_neutral,charge_neutral,0);        
    end    
    
    if(PLOT_KE)        
        %[ke,leg_5]   = kineticEnergy(fig_5,time,velocityX,velocityY,velocityZ);   
        ke = sum(0.5.*density.*(velocityX.^2 + velocityY.^2 + velocityZ.^2))/N_PARTICLE       
        
        plot(fig_5,time,ke,'ro');     
        %plot(fig_5,time,10*time^(-5/3),'k+');
        set(fig_5,'ylim',[0 0.14]);        
        
        if(PLOT_DISSIPATION_RATE)
            ke_sim_v2(iter) = ke;
            t_sim_v2(iter)  = time;
        end
    end    
    
    if(SINGLE_PARTICLE_IN_BOX)        
        trajectory(fig_0,positionX,positionY,positionZ,100,step);
    end
    
    if(PLOT_CHARGE_VS_TIME)
        [q] = chargeVsTime(fig_6,charge,time);
    end
    
    if(PLOT_MSD)
        if step==1
            X_0 = positionX;Y_0 = positionY;Z_0 = positionZ;
        end
        [dr_msd,t_msd] = msd(fig_16,positionX,positionY,positionZ,X_0,Y_0,Z_0,step,time);
    end

    if(CLUSTER)               
        clusterMain;        
    end
    
    %flushVars(timestep, time, timestring,ke, charge);    

%     forceX_ela = getVector(simulation_output_directory, 'forceX_elastic_t', timestring, FLOATTYPE);
%     forceY_ela = getVector(simulation_output_directory, 'forceY_elastic_t', timestring, FLOATTYPE);
%     forceZ_ela = getVector(simulation_output_directory, 'forceZ_elastic_t', timestring, FLOATTYPE);
%     
%     forceX_dis = getVector(simulation_output_directory, 'forceX_dissipative_t', timestring, FLOATTYPE);
%     forceY_dis = getVector(simulation_output_directory, 'forceY_dissipative_t', timestring, FLOATTYPE);
%     forceZ_dis = getVector(simulation_output_directory, 'forceZ_dissipative_t', timestring, FLOATTYPE);
%     
%     forceX_cou = getVector(simulation_output_directory, 'forceX_coulomb_t', timestring, FLOATTYPE);
%     forceY_cou = getVector(simulation_output_directory, 'forceY_coulomb_t', timestring, FLOATTYPE);
%     forceZ_cou = getVector(simulation_output_directory, 'forceZ_coulomb_t', timestring, FLOATTYPE);
%     
%     
%     
%     ftot_dot_v = (forceX.*velocityX + forceY.*velocityY + forceZ.*velocityZ);
%     
%     fela_dot_v = (forceX_ela.*velocityX + forceY_ela.*velocityY + forceZ_ela.*velocityZ);
%     fdis_dot_v = (forceX_dis.*velocityX + forceY_dis.*velocityY + forceZ_dis.*velocityZ);
%     fcou_dot_v = (forceX_cou.*velocityX + forceY_cou.*velocityY + forceZ_cou.*velocityZ);
%     
%     fela_dot_fcou = (forceX_ela.*forceX_cou + forceY_ela.*forceY_cou + forceZ_ela.*forceZ_cou);
%     
%     ftot_minus_fela_X = forceX - forceX_ela;
%     ftot_minus_fela_Y = forceY - forceY_ela;
%     ftot_minus_fela_Z = forceZ - forceZ_ela;
%     
%     
%     
% %     plot(fig_50,time,mean(ftot_dot_v),'og');
% %     plot(fig_50,time,mean(fela_dot_v),'ok');
% %     plot(fig_50,time,mean(fdis_dot_v),'ob');
% %     plot(fig_50,time,mean(fcou_dot_v),'or');
%     
%     plot(fig_50,time,mean(ftot_minus_fela_X.*velocityX),'+r');
    %plot(fig_50,time,max(ftot_dot_v),'*r');
    
    %set(fig_50,'xscale','log');
    %set(fig_50,'yscale','log');

    %set(gca,'ylim',[-0.1 0.1]);
    
end
%--------------------------------------------------------------------------
    pause(1e-1);
    save(sprintf('%s/t_simulation.mat',matlab_output_directory),'t_simulation');
    save(sprintf('%s/Ma_simulation.mat',matlab_output_directory),'Ma_simulation');   

    figure;
    dt = DELTA_T*ITER_DATA_GEN;
    e = gradient(Ma_simulation,dt);
    plot(gca,t_simulation,e,'ob','linewidth',2);
    set(gca,'xscale','log');
    set(gcs,'yscale','log');
    %set(gca,'ylim',[0 0.016]);

%--------------------------------------------------------------------------
if(PLOT_DISSIPATION_RATE)
    dt = DELTA_T*ITER_DATA_GEN;
    e  = -gradient(ke_sim_v2,dt);
    plot(fig_7,t_sim_v2,e,'-b','linewidth',2);
    set(fig_7,'ylim',[0 0.016]);
end

%--------------------------------------------------------------------------
if(PLOT_V_DIST == 1)
    pdf(fig_9,velocityX); 
end

%--------------------------------------------------------------------------
if(PLOT_CHARGE_DIST == 1)
   pdf(fig_10,charge); 
end
%--------------------------------------------------------------------------

if(PLOT_F_DIST == 1)
    pdf(fig_8,forceX);    
end
%--------------------------------------------------------------------------
if (MOVIE==1)
    close(video_name);
end
%--------------------------------------------------------------------------
%     pause(1.0);
%     set(fig_6_gcf,'PaperUnits','centimeters','PaperPosition',[2 2 20 16])
%     pause(1.0);
%     outputFileName = sprintf('charge_vs_time');
%     print(fig_6_gcf,outputFileName,'-dpdf');
%     pause(1.0);

%--------------------------------------------------------------------------
if(AVG_CLUSTER_TEMP == 1)

    pause(1e-1);
    save(sprintf('%s/t_simulation.mat',matlab_output_directory),'t_simulation');
    save(sprintf('%s/mean_Df.mat',matlab_output_directory),'mean_Df');   

    pause(1e-1);
    save(sprintf('%s/max_cls_Df.mat',matlab_output_directory),'max_cls_Df');
    save(sprintf('%s/lower_Df.mat',matlab_output_directory),'lower_Df');
    save(sprintf('%s/upper_Df.mat',matlab_output_directory),'upper_Df');    
    
    pause(1e-1);
    save(sprintf('%s/T_simulation.mat',matlab_output_directory),'T_simulation');    
    save(sprintf('%s/dqq_simulation.mat',matlab_output_directory),'dqq_simulation');
    
    pause(1e-4);
    
    save(sprintf('%s/n_simulation.mat',matlab_output_directory),'n_simulation');
    save(sprintf('%s/n_simulation_mean_field.mat',matlab_output_directory),'n_simulation_mean_field');
    save(sprintf('%s/R_simulation.mat',matlab_output_directory),'R_simulation');
    
    pause(1e-4);
    save(sprintf('%s/avg_cluster_mass_simulation.mat',matlab_output_directory),'avg_cluster_mass_simulation');
    save(sprintf('%s/avg_cluster_size_simulation.mat',matlab_output_directory),'avg_cluster_size_simulation');
    
    pause(1e-4);
    save(sprintf('%s/max_cluster_mass_simulation.mat',matlab_output_directory),'max_cluster_mass_simulation');
    save(sprintf('%s/max_cluster_size_simulation.mat',matlab_output_directory),'max_cluster_size_simulation');     
    
end