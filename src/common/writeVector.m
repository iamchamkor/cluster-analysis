function vector = writeVector(foldername, filename, timestring, FLOATTYPE, N_PARTICLE)

    readFileName = sprintf('%s/%s%s.dat', foldername, filename, timestring);
    fileID = fopen(readFileName,'wb');
    vec = fwrite(fileID,[1 N_PARTICLE],FLOATTYPE);
    fclose(fileID);
    vector=vec';
    
end
