function [fig_1,fig_1_gcf] = createFig(PARTICLES_IN_BOX,xlabel,ylabel,holdcondition,boxcondition,scale,fontsize)

if(PARTICLES_IN_BOX==1)   
    
    figure; fig_1 = axes; fig_1_gcf = gcf;    
    
    if(strcmp(holdcondition,'holdon'))
        hold(fig_1,'on');
    elseif(strcmp(holdcondition,'holdoff'))
        hold(fig_1,'off');
    else
        %do nothing
    end
    
    if(strcmp(boxcondition,'boxon'))
        box(fig_1,'on');
    elseif(strcmp(boxcondition,'boxoff'))
        box(fig_1,'off');
    else
        %do nothing
    end
    
    set(fig_1,'fontsize',fontsize,'fontname','times');
    
    if(strcmp(scale,'linlin'))
        setAxisLabel(fig_1,xlabel,ylabel,fontsize, 'linear', 'linear');
    elseif(strcmp(scale,'linlog'))
        setAxisLabel(fig_1,xlabel,ylabel,fontsize, 'linear', 'log');
    elseif(strcmp(scale,'loglin'))
        setAxisLabel(fig_1,xlabel,ylabel,fontsize, 'log', 'lin');
    elseif(strcmp(scale,'loglog'))
        setAxisLabel(fig_1,xlabel,ylabel,fontsize, 'log', 'log');
    else
        setAxisLabel(fig_1,xlabel,ylabel,fontsize, 'log', 'log');
    end
        
else
    fig_1 = 0; fig_1_gcf = 0;
end
end