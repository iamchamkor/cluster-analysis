function [] = makeMovie(fig_gcf_handle,aviobj)

    if(fig_gcf_handle~=0)
        set(fig_gcf_handle,'visible','on');
        frame =getframe(fig_gcf_handle);
        writeVideo(aviobj, frame);
    end
    
end