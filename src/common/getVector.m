function vector = getVector(foldername, filename, timestring, FLOATTYPE)

    readFileName = sprintf('%s/%s%s.dat', foldername, filename, timestring);
    fileID = fopen(readFileName,'r');
    vec = fread(fileID,[1 Inf],FLOATTYPE);
    fclose(fileID);
    vector=vec';
    
end
