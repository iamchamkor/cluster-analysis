function [] = printpdf(handle, filename)        
    
    %set(handle,'PaperUnits','centimeters','PaperPosition',[2 2 20 20]);    
    
    print(handle,filename,'-dpdf','-r0');    
      
end