clc;
close all;
clear all;

q=zeros(2,1);
r=zeros(2,1);
E=zeros(2,1);
mu=zeros(2,1);

q(1) = -1; q(2) = +1;
r(1) = -1; r(2) = +1;
E(1) =  0; E(2) =  0;
mu(1)=  0; mu(2)=  0; 

dummy = 0.0;
err = 0.0;
err_p = 0.0;

while 1
  
    err = 0.0;
    for i=1:2
        for j=1:2
            if(i~=j)
                
                rij = r(j) - r(i);
                E(i)= - (q(j)*rij)/abs(rij)^3  - (3*rij*rij*mu(j) - mu(j)*rij^2)/abs(rij)^5;
   
                dummy = mu(i);
                mu(i)= 0.1*E(i);    
                if abs(dummy-mu(i))>err
                    err = abs(dummy-mu(i));
                end
            end
        end
    end
   
    err
    if err<1e-10
        break;
    end
    
end

    for i=1:2
        for j=1:2
            if(i~=j)
                
                rij = r(j) - r(i);
                F(i)= - (q(i)*q(j)*rij)/abs(rij)^3  + (q(i)*mu(j)-q(j)*mu(i))/abs(rij)^3 - 3*rij*(q(i)*rij*mu(j)-q(j)*rij*mu(i))/abs(rij)^5; %CC,DC,CD
                
                a = 3.0/abs(rij)^5;
                b = -15/abs(rij)^7;
                F(i)= F(i) + (  -mu(i)*(3*a*mu(j)*rij + b*mu(j)*rij^3) );  
                
            end
        end
    end

for i=1:2
fprintf("E(%d)=%.10f\n",i,E(i));
end

for i=1:2
fprintf("F(%d)=%.10f\n",i,F(i));
end
