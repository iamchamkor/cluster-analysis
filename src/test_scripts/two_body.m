clc
close all
clear all

x=zeros(2,1);
v=zeros(2,1);
f=zeros(2,1);
m=zeros(2,1);
q=zeros(2,1);

dt=0.01;

%initial conditions
x(1)=-0.6;
x(2)=+0.6;
v(1)=+1;
v(2)=-1;
q(1)=0;
q(2)=0;
m(1)=1;
m(2)=1;

for n=1:100000

for i=1:2
	v(i) = v(i) + 0.5*f(i)*dt/m(i);
end

for i=1:2
	x(i) = x(i) + v(i)*dt;
end

overlap = 1.0-(x(2)-x(1))
vn      = v(2)- v(1);

if overlap>0.0
fel  =  227.0*(overlap)^(1.5);
fds  = +027*vn*(overlap)^(0.5);
else
fel  = 0;
fds  = 0;
end

ft   = -fel+fds;

if ft<0
f(1) = +ft;
f(2) = -ft;
else
f(1) = 0.0;
f(2) = 0.0;
end

fcl  = q(1)*q(2)/(x(2)-x(1))^2;
f(1) = f(1) - fcl;
f(2) = f(2) + fcl; 

for i=1:2
	v(i) = v(i) + 0.5*f(i)*dt/m(i);
end

if overlap>0.0
	Iij  = randn();
		if abs(q(1) + Iij*sqrt(dt))<1.0 && abs(q(2) - Iij*sqrt(dt))<1.0
			q(1) = q(1) + Iij*sqrt(dt);
			q(2) = q(2) - Iij*sqrt(dt);
		end
end

q(1)
q(2)

E  = mean(0.5.*m.*v.^2);
EH = 0.5*(n*dt)^(-5/3);

%plot(n*dt,E,'o');hold on;
%plot(n*dt,overlap,'*');
%plot(n*dt,EH,'o');ylim(gca,[0,1]);
%set(gca,'xscale','log');set(gca,'yscale','log');
plot(x,[0,0],'o','markersize',218);xlim(gca,[-1,1]);
pause(1e-8)

end
