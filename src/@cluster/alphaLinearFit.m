function [Alpha_fit, v_cls_fit] = alphaLinearFit(cluster, no_of_particles_in_ith_cluster, v_cls)
            
if(length(no_of_particles_in_ith_cluster)>1)
        F = polyfit(log(no_of_particles_in_ith_cluster),log(v_cls),1);
        slope     = F(1);
        intercept = F(2);     
        
        Alpha_fit = 1/slope;
                
        v_cls_fit = no_of_particles_in_ith_cluster.^(1/Alpha_fit).*exp(intercept);                        
else
   Alpha_fit = 0;
   v_cls_fit = 0;
end
            
end            