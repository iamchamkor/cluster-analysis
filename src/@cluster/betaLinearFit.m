function [Beta_fit, q_cls_fit] = betaLinearFit(cluster, no_of_particles_in_ith_cluster, q_cls)
            
if(length(no_of_particles_in_ith_cluster)>1)
        F = polyfit(log(no_of_particles_in_ith_cluster),log(q_cls),1);
        slope     = F(1);
        intercept = F(2);     
        
        Beta_fit = 1/slope;
                
        q_cls_fit = no_of_particles_in_ith_cluster.^(1/Beta_fit).*exp(intercept);                        
else
   Beta_fit  = 0;
   q_cls_fit = 0;
end
            
end            