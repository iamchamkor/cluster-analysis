function [mean_x,mean_y,mean_z,mean_vx,mean_vy,mean_vz,mean_q,mean_mux,mean_muy,mean_muz,N] = cluster2particle(clusters, cid)

    mean_x = mean(clusters{1,cid}(clusters{4,cid}));
    mean_y = mean(clusters{2,cid}(clusters{4,cid}));
    mean_z = mean(clusters{3,cid}(clusters{4,cid}));

    mean_vx = mean(clusters{5,cid}(clusters{4,cid}));
    mean_vy = mean(clusters{6,cid}(clusters{4,cid}));
    mean_vz = mean(clusters{7,cid}(clusters{4,cid}));
      
    mean_q  = mean(clusters{8,cid}(clusters{4,cid}));
    
    mean_mux = mean(clusters{9,cid}(clusters{4,cid}));
    mean_muy = mean(clusters{10,cid}(clusters{4,cid}));
    mean_muz = mean(clusters{11,cid}(clusters{4,cid}));    
    
    N=length(clusters{4,cid});
          
end