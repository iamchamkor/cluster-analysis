function [T_agg] = temperature2(clusters, cid)

    mean_vx = mean(clusters{5,cid}(clusters{4,cid}));
    mean_vy = mean(clusters{6,cid}(clusters{4,cid}));
    mean_vz = mean(clusters{7,cid}(clusters{4,cid}));
    
    v_mean = [mean_vx, mean_vy, mean_vz];
      
    velocity_mat = [clusters{5,cid}(clusters{4,cid}), clusters{6,cid}(clusters{4,cid}), clusters{7,cid}(clusters{4,cid})];
        
    rel_velocity = velocity_mat - v_mean;    
    
    N=length(clusters{4,cid});
        
    T_agg = (1/3)*sum( rel_velocity(:,1).^2 + rel_velocity(:,2).^2 + rel_velocity(:,3).^2 )/N;    

end