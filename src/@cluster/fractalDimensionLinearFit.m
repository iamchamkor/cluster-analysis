function [Df_fit,Rg_fit] = fractalDimensionLinearFit(cluster, no_of_particles_in_ith_cluster, Rg)
            
if(length(no_of_particles_in_ith_cluster)>1)
        F = polyfit(log(no_of_particles_in_ith_cluster),log(Rg),1);
        slope     = F(1);
        intercept = F(2);     
        
        Df_fit = 1/slope;
                
        Rg_fit = no_of_particles_in_ith_cluster.^(1/Df_fit).*exp(intercept);                        
else
   Df_fit = 0;
   Rg_fit = 0;
end
            
end            