function [T_agg] = temperature(clusters, cid)    
    
    global CELL_LENGTH;

    positionX=clusters{1,cid}(clusters{4,cid});
    positionY=clusters{2,cid}(clusters{4,cid});
    positionZ=clusters{3,cid}(clusters{4,cid});
           
    velocityX=clusters{5,cid}(clusters{4,cid});
    velocityY=clusters{6,cid}(clusters{4,cid});
    velocityZ=clusters{7,cid}(clusters{4,cid});

    N_PARTICLE = length(positionX);
                                                                                                                               
        nboxes=1;
        
        np_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumX_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumY_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumZ_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        
        fluctuating_velocityX = velocityX - velocityX;
        fluctuating_velocityY = velocityY - velocityY;
        fluctuating_velocityZ = velocityZ - velocityZ;
        
        for(pid=1:N_PARTICLE)
            ibox=floor( (positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                np_box(ibox+1,jbox+1,kbox+1) = np_box(ibox+1,jbox+1,kbox+1)+1;
                momentumX_box(ibox+1,jbox+1,kbox+1) = momentumX_box(ibox+1,jbox+1,kbox+1) + 1*velocityX(pid);
                momentumY_box(ibox+1,jbox+1,kbox+1) = momentumY_box(ibox+1,jbox+1,kbox+1) + 1*velocityY(pid);
                momentumZ_box(ibox+1,jbox+1,kbox+1) = momentumZ_box(ibox+1,jbox+1,kbox+1) + 1*velocityZ(pid);
            end
        end        

        com_velocityX_box = momentumX_box./np_box;
        com_velocityY_box = momentumY_box./np_box;
        com_velocityZ_box = momentumZ_box./np_box;

        com_velocityX_box(isnan(com_velocityX_box))=0;
        com_velocityY_box(isnan(com_velocityY_box))=0;
        com_velocityZ_box(isnan(com_velocityZ_box))=0;
        
        for(pid=1:N_PARTICLE)
            ibox=floor( (positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                fluctuating_velocityX(pid) = velocityX(pid) - com_velocityX_box(ibox+1,jbox+1,kbox+1);
                fluctuating_velocityY(pid) = velocityY(pid) - com_velocityY_box(ibox+1,jbox+1,kbox+1);
                fluctuating_velocityZ(pid) = velocityZ(pid) - com_velocityZ_box(ibox+1,jbox+1,kbox+1);                                
            end
        end                
       
        v_abs_2    = (fluctuating_velocityX.^2 + fluctuating_velocityY.^2 + fluctuating_velocityZ.^2);
        v_abs_2    = sum(v_abs_2)/N_PARTICLE;
        ke         = (1/2)*1.0*v_abs_2;        
        T_agg      = (2/3)*ke;
        
        T_agg(isnan(T_agg))=0;
       
end