classdef cluster < handle
    
    properties
        
            CLUSTER;
        
            CONTACT_DISTANCE;        
            MINIMUM_PARTICLES;
            MAXIMUM_PARTICLES;
        
            no_of_clusters;
            avg_cluster_mass;
            avg_cluster_size;
            max_cluster_mass;
            max_cluster_size;
            no_of_particles_in_ith_cluster;
            clusters;
            cid_maxsize;
            Rg;
            Df;
            Df_fit;
            T_cls;
            q_cls;  
            
            v_cls_abs; %cluster absolute velocity           
            Alpha_fit; %from v_cls vs mass fit
            
            q_cls_abs; %cluster absolute charge
            Beta_fit; %from q_cls_abs vs mass fit
            
            mom_cls_abs; %cluster absolute dipole moment   
            var_mom_cls; %cluster moment variance
            
            pdf_cluster_mass;
            avg_cluster_mass_M2byM1;
    
            pdf_charge_monomers={};
            pdf_charge_clusters;
            pdf_charge_largest_cluster;
            
            var_charge_monomers;
            var_charge_clusters;
            
            K_clusters;
            
        %|cluster-switches|also indicate exisisting functionalities|%%%%%%%     
            MASS_VS_RG                   ; fig_37; fig_37_gcf;
            CLUSTER_HISTOGRAM            ; fig_25; fig_25_gcf;  
            CLUSTER_PDF                  ; fig_26; fig_26_gcf;
            AVG_FRACTAL_DIM              ; fig_27; fig_27_gcf;
            AVG_CLUSTER_SIZE             ; fig_28; fig_28_gcf;
            SHOW_MAX_CLUSTER             ; fig_24; fig_24_gcf;
            SHOW_CLUSTER_MIN_PARTICLES   ; fig_29; fig_29_gcf;
            FRACTAL_DIMENSION_HISTOGRAM  ; fig_30; fig_30_gcf;
            AVG_CLUSTER_TEMP             ; fig_33; fig_33_gcf;
            PLOT_CLUSTER_CHARGE_VAR      ; fig_31; fig_31_gcf;
            PLOT_CLUSTER_NUMBER_DENSITY  ; fig_32; fig_32_gcf;
            CLUSTER_K                    ; fig_34; fig_34_gcf;
            CLUSTER_RADIUS_OF_GYRATION   ; fig_35; fig_35_gcf;

            CHARGE_PDF_NEWBORN_PARTICLES ; fig_36; fig_36_gcf;
            CHARGE_PDF_LARGEST_CLUSTER   ; fig_38; fig_38_gcf;
            CHARGE_PDF_ALL_PARTICLES     ; fig_39; fig_39_gcf;
            CHARGE_ORDER_PARAMETER       ; fig_cls_01; fig_cls_01_gcf;
            
            MASS_VS_VELOCITY             ; fig_cls_02; fig_cls_02_gcf;
            MASS_VS_CHARGE               ; fig_cls_03; fig_cls_03_gcf;
            
            SHOW_COLORED_CLUSTERS        ; fig_cls_04; fig_cls_04_gcf;
            SHOW_CLUSTER_MOMENTS         ; %dont need a separate fig handle
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
        
    end
    
    methods
        
        function [obj] = cluster %class-constructor
            
            obj.CLUSTER = 1;

            if(obj.CLUSTER)
                
            obj.CONTACT_DISTANCE  = 1;        
            obj.MINIMUM_PARTICLES = 3;
            obj.MAXIMUM_PARTICLES = 10^6;
                        
            %|cluster-switches|also indicate exisisting functionalities|%%%
            obj.MASS_VS_RG                   = 0;[obj.fig_37,obj.fig_37_gcf]  = createFig(obj.MASS_VS_RG,                       '$m$',                                  '$R_g$',         'holdon',   'boxon',   'loglog',20); 
            obj.CLUSTER_HISTOGRAM            = 0;[obj.fig_25,obj.fig_25_gcf]  = createFig(obj.CLUSTER_HISTOGRAM,                '$m$',                                  '$N_m$',         'holdon',   'boxon',   'loglog',14);  
            obj.CLUSTER_PDF                  = 0;[obj.fig_26,obj.fig_26_gcf]  = createFig(obj.CLUSTER_PDF,                      '$m/\sigma$',                           '$f(m/\sigma)$', 'holdoff',  'boxon',   'loglog',14);
            obj.AVG_FRACTAL_DIM              = 0;[obj.fig_27,obj.fig_27_gcf]  = createFig(obj.AVG_FRACTAL_DIM,                  '$t v_\mathrm{ref}/d$',                 '$D_{f,avg}$',   'holdon',   'boxon',   'linlin',20);
            obj.AVG_CLUSTER_SIZE             = 0;[obj.fig_28,obj.fig_28_gcf]  = createFig(obj.AVG_CLUSTER_SIZE,                 '$t v_\mathrm{ref}/d$',                 '$S(t)$',        'holdon',   'boxon',   'loglog',14);
            obj.SHOW_MAX_CLUSTER             = 0;[obj.fig_24,obj.fig_24_gcf]  = createFig(obj.SHOW_MAX_CLUSTER ,                '','','holdon','','linlin',14);
            obj.SHOW_CLUSTER_MIN_PARTICLES   = 0;[obj.fig_29,obj.fig_29_gcf]  = createFig(obj.SHOW_CLUSTER_MIN_PARTICLES ,      '',                                     '',              'holdon',   '',        'linlin',14);
            obj.FRACTAL_DIMENSION_HISTOGRAM  = 0;[obj.fig_30,obj.fig_30_gcf]  = createFig(obj.FRACTAL_DIMENSION_HISTOGRAM,      '$D_f$',                                '$n(D_f)$',      'holdon',   'boxon',   'loglog',20);
            obj.AVG_CLUSTER_TEMP             = 0;[obj.fig_33,obj.fig_33_gcf]  = createFig(obj.AVG_CLUSTER_TEMP,                 'time $t v_\textrm{ref}/d$',            '$T$',           'holdon',   'boxon',   'loglog',14);
            obj.PLOT_CLUSTER_CHARGE_VAR      = 0;[obj.fig_31,obj.fig_31_gcf]  = createFig(obj.PLOT_CLUSTER_CHARGE_VAR,          '$t v_\mathrm{ref}/d$',                 '$<\delta q^2>$','holdon',   'boxon',   'loglog',20);
            obj.PLOT_CLUSTER_NUMBER_DENSITY  = 0;[obj.fig_32,obj.fig_32_gcf]  = createFig(obj.PLOT_CLUSTER_NUMBER_DENSITY,      '$t v_\mathrm{ref}/d$',                 '$n$',           'holdon',   'boxon',   'loglog',20);
            obj.CLUSTER_K                    = 0;[obj.fig_34,obj.fig_34_gcf]  = createFig(obj.CLUSTER_K,                        'time $t v_\textrm{ref}/d$',            '$K=k_e \langle \delta q^2 \rangle / (Td)$',           'holdon',   'boxon',   'loglog',14);
            obj.CLUSTER_RADIUS_OF_GYRATION   = 0;[obj.fig_35,obj.fig_35_gcf]  = createFig(obj.CLUSTER_RADIUS_OF_GYRATION,       'time $t v_\textrm{ref}/d$',            '$<R_g>$',       'holdon',   'boxon',   'loglog',14);
           
            obj.CHARGE_PDF_ALL_PARTICLES     = 0;[obj.fig_39,obj.fig_39_gcf]  = createFig(obj.CHARGE_PDF_ALL_PARTICLES,         '$q_{monomers}/\langle \delta q^2 \rangle^{1/2}$',   '$f(q_{monomers})$',        'holdon',   'boxon',   'linlog',14);           
            obj.CHARGE_PDF_NEWBORN_PARTICLES = 0;[obj.fig_36,obj.fig_36_gcf]  = createFig(obj.CHARGE_PDF_NEWBORN_PARTICLES,     '$q_{clusters}/\langle \delta q^2 \rangle^{1/2}$',   '$f(q_{clusters})$',        'holdon',   'boxon',   'linlog',14);
            obj.CHARGE_PDF_LARGEST_CLUSTER   = 0;[obj.fig_38,obj.fig_38_gcf]  = createFig(obj.CHARGE_PDF_LARGEST_CLUSTER ,      '$q_{maxcluster}/\langle \delta q^2 \rangle^{1/2}$', '$f(q_{maxcluster})$',      'holdon',   'boxon',   'linlog',14);
            
            obj.CHARGE_ORDER_PARAMETER       = 0;[obj.fig_cls_01,obj.fig_cls_01_gcf]  = createFig(obj.CHARGE_ORDER_PARAMETER,   'time $t v_\textrm{ref}/d$',            '$\langle|\sum_{i=1}^{N_j} q_i|\rangle_j$',       'holdon',   'boxon',   'loglog',14);
            
            obj.MASS_VS_VELOCITY             = 0;[obj.fig_cls_02,obj.fig_cls_02_gcf]  = createFig(obj.MASS_VS_VELOCITY,         '$m$',                                  '$v$',         'holdon',   'boxon',   'loglog',20);
            obj.MASS_VS_CHARGE               = 0;[obj.fig_cls_03,obj.fig_cls_03_gcf]  = createFig(obj.MASS_VS_CHARGE,           '$m$',                                  '$q$',         'holdon',   'boxon',   'loglog',20);
            
            obj.SHOW_COLORED_CLUSTERS        = 1;[obj.fig_cls_04,obj.fig_cls_04_gcf]  = createFig(obj.SHOW_COLORED_CLUSTERS ,      '',                                     '',              'holdon',   '',        'linlin',14);
            obj.SHOW_CLUSTER_MOMENTS         = 0;%should be switched off if obj.SHOW_COLORED_CLUSTERS=0
            
            %|cluster-switches|also indicate exisisting functionalities|%%%
  
            end
            
        end
        
    end
    
end