% this script is called at the beginning of cluster main.
% turns on or off the functionality, and generates desired figure handles. 

% CLUSTER                      = 1;


MASS_VS_RG                   = 0;    %plots mass vs radius of gyration.
CLUSTER_HISTOGRAM            = 0;    
CLUSTER_PDF                  = 0;
AVG_FRACTAL_DIM              = 0;
AVG_CLUSTER_SIZE             = 0;
SHOW_MAX_CLUSTER             = 0;
SHOW_CLUSTER_MIN_PARTICLES   = 0;
FRACTAL_DIMENSION_HISTOGRAM  = 0;
AVG_CLUSTER_TEMP             = 0;
PLOT_CLUSTER_CHARGE_VAR      = 0;
PLOT_CLUSTER_NUMBER_DENSITY  = 0;
CLUSTER_R                    = 0;
CLUSTER_RADIUS_OF_GYRATION   = 0;

CHARGE_PDF_NEWBORN_PARTICLES = 0;
CHARGE_PDF_LARGEST_CLUSTER   = 0;
CHARGE_PDF_ALL_PARTICLES     = 0;
CHARGE_ORDER_PARAMETER       = 1;


[fig_24,fig_24_gcf]  = createFig(SHOW_MAX_CLUSTER ,           '',                           '',              'holdon',   '',        'linlin',14);
[fig_25,fig_25_gcf]  = createFig(CLUSTER_HISTOGRAM,           '$s$',                        '$N(s)$',        'holdon',   'boxon',   'loglog',14);       
[fig_26,fig_26_gcf]  = createFig(CLUSTER_PDF,                 '$m/\sigma$',                 '$f(m/\sigma)$', 'holdoff',  'boxon',   'loglog',14);                
[fig_27,fig_27_gcf]  = createFig(AVG_FRACTAL_DIM,             '$t v_\mathrm{ref}/d$',       '$D_{f,avg}$',   'holdon',   'boxon',   'linlin',20);   
[fig_28,fig_28_gcf]  = createFig(AVG_CLUSTER_SIZE,            '$t v_\mathrm{ref}/d$',       '$S(t)$',        'holdon',   'boxon',   'loglog',14); 
[fig_37,fig_37_gcf]  = createFig(MASS_VS_RG,                  '$m$',                        '$R_g$',         'holdon',   'boxon',   'loglog',20); 
[fig_29,fig_29_gcf]  = createFig(SHOW_CLUSTER_MIN_PARTICLES , '',                           '',              'holdon',   '',        'linlin',14);
[fig_30,fig_30_gcf]  = createFig(FRACTAL_DIMENSION_HISTOGRAM, '$D_f$',                      '$n(D_f)$',      'holdon',   'boxon',   'loglog',20);    
[fig_31,fig_31_gcf]  = createFig(PLOT_CLUSTER_CHARGE_VAR,     '$t v_\mathrm{ref}/d$',       '$<\delta q^2>$','holdon',   'boxon',   'loglog',20); 
[fig_32,fig_32_gcf]  = createFig(PLOT_CLUSTER_NUMBER_DENSITY, '$t v_\mathrm{ref}/d$',       '$n$',           'holdon',   'boxon',   'loglog',20); 
[fig_33,fig_33_gcf]  = createFig(AVG_CLUSTER_TEMP,            'time $t v_\textrm{ref}/d$',  '$T$',           'holdon',   'boxon',   'loglog',14);
[fig_34,fig_34_gcf]  = createFig(CLUSTER_R,                   'time $t v_\textrm{ref}/d$',  '$R$',           'holdon',   'boxon',   'loglog',14);
[fig_35,fig_35_gcf]  = createFig(CLUSTER_RADIUS_OF_GYRATION,  'time $t v_\textrm{ref}/d$',  '$<R_g>$',       'holdon',   'boxon',   'loglog',14);

[fig_37,fig_37_gcf]  = createFig(CHARGE_PDF_NEWBORN_PARTICLES,'$q/\langle \delta q^2 \rangle^{1/2}$',  '$f(q)$',       'holdon',   'boxon',   'linlog',14);
[fig_38,fig_38_gcf]  = createFig(CHARGE_PDF_LARGEST_CLUSTER ,'$q/\langle \delta q^2 \rangle^{1/2}$',  '$f(q)$',       'holdon',   'boxon',   'linlog',14);
[fig_39,fig_39_gcf]  = createFig(CHARGE_PDF_ALL_PARTICLES,'$q/\langle \delta q^2 \rangle^{1/2}$',  '$f(q)$',       'holdon',   'boxon',   'linlog',14);

[fig_cls_01,fig_cls_01_gcf]  = createFig(CHARGE_ORDER_PARAMETER,'time $t v_\textrm{ref}/d$',  '$\langle|\sum_{i=1}^{N_j} q_i|\rangle_j$',       'holdon',   'boxon',   'loglog',14);