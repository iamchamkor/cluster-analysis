function [xi,f] = pdf(fig_handle,x)


    xo      = std(x);
    
    x_star  = x/xo;    
    
    x_gaus  = normpdf(x_star);
   
    %plot(fig_handle, x_star, x_gaus,'ko','markersize',3);   
    
    [f,xi] = ksdensity(x_star);
    
    plot(fig_handle, xi, f,'.','linewidth',2,'markersize',7);
%     
%     set(fig_handle,'yscale','log');
%     
    set(fig_handle,'xlim',[-6 +6]);
    set(fig_handle,'ylim',[+1e-4 +1e+1]);

    

%     xo      = std(x);
% 
%     x       = x/xo; 
% 
% 	N       = length(x);   
% 	
%     nbins   = round(sqrt(N));
%     
%     [Count,xi] = hist(x,nbins);
%    
% 	Pi = Count/trapz(xi,Count);
% 
%  	plot(fig_handle,xi, Pi, '-r');
%     
%     set(fig_handle,'yscale','log');
    
    %set(fig_handle,'ylim',[+1e-5 +1e+0]);
    
end
