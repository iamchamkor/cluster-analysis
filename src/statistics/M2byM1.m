function [M2_by_M1] = M2byM1(x, Nx)

    M2 = sum(x.^2 .* Nx);
    M1 = sum(x    .* Nx);

    M2_by_M1 = M2/M1;

end