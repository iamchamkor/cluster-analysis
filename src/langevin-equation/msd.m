function [MSD,s] = msd(x,dt)
for n = 0:1:sqrt(length(x))
    MSD(n+1) = mean((x(n+1:end)-x(1:end-n)).^2);
end
    s = dt*[0:1:length(MSD)-1];
end