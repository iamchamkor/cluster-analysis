function [x,y,z,tt] = integrate_charged(dt,N)

x=zeros(N,1);
y=zeros(N,1);
z=zeros(N,1);

%%%%%%%%%%%%%%%%%%%%
m       = 1.0;
T_o     = 1.0;
gamma_o = 1.0;

a       = 1/6; 
tau     = 10^5; 
%%%%%%%%%%%%%%%%%%%%
x(1)=0;
y(1)=0;
z(1)=0;                 
% initial conditions
x(2)=0;
y(2)=0;
z(2)=0;
%%%%%%%%%%%%%%%%%%%%

for i = 3:1:N

    % scaled motion
    t = i*dt;
    
    T     =     T_o*(1+t/tau)^(2*a-2);
    gamma =     gamma_o*(1+t/tau)^(1*a-1);
    D     =     T/(gamma*m);
    
    alpha = gamma;
    beta  = sqrt(2*D)*gamma;
    
    %%%%%%%%%%%%%%%%
    
    eta_x = randn();
    eta_y = randn();
    eta_z = randn();
    
    tau_el = 10^3;
    
    x(i) = x(i-1)*(2+dt*alpha/m)/(1+dt*alpha/m) ...
          -x(i-2)*(1)/(1+dt*alpha/m) ...
          +eta_x * (dt)^(3/2) * beta/(m*(1+dt*alpha/m)) + (exp(-t/tau_el)-1) * eta_x * (dt)^(3/2) * beta/(m*(1+dt*alpha/m)) + exp(-t/tau_el) * randn() * (dt)^(3/2) /(m*(1+dt*alpha/m));
      
    y(i) = y(i-1)*(2+dt*alpha/m)/(1+dt*alpha/m) ...
          -y(i-2)*(1)/(1+dt*alpha/m) ...
          +eta_y * (dt)^(3/2) * beta/(m*(1+dt*alpha/m)) + (exp(-t/tau_el)-1) * eta_y * (dt)^(3/2) * beta/(m*(1+dt*alpha/m)) + exp(-t/tau_el) * randn() * (dt)^(3/2) /(m*(1+dt*alpha/m));
      
    z(i) = z(i-1)*(2+dt*alpha/m)/(1+dt*alpha/m) ...
          -z(i-2)*(1)/(1+dt*alpha/m) ...
          +eta_z * (dt)^(3/2) * beta/(m*(1+dt*alpha/m)) + (exp(-t/tau_el)-1) * eta_z * (dt)^(3/2) * beta/(m*(1+dt*alpha/m)) + exp(-t/tau_el) * randn() * (dt)^(3/2) /(m*(1+dt*alpha/m));      
      
    
    
end

tt = 0:dt:(N-1)*dt;

end