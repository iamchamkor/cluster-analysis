clear all;
close all;
clc;

dt = 1.0;
N  = 1e8;

ensamble_size = 5;
MSD_ensamble=zeros(N,1);

for k=1:ensamble_size
[x,y,z,t] = integrate_charged(dt,N);

% figure;
% plot3(x,y,z);
% xlabel('x');
% ylabel('y');
% zlabel('z');
% axis equal;

%[MSD,s] = msd(x,dt);
[MSD,s] = msd_absolute(x,y,z,t);

MSD_ensamble = MSD_ensamble + MSD;

end

MSD = MSD_ensamble./ensamble_size;

figure; fig_1 = axes; fig_1_gcf = gcf; hold(fig_1,'on'); box(fig_1,'on');

plot(s,MSD);
xlabel('s');
ylabel('MSD');
set(fig_1,'xscale','log');
set(fig_1,'yscale','log');

t = 1:10:10^2;
M1= t.^(2);
plot(fig_1,t,M1,'k--','markerfacecolor','white','markersize',5);

t = 10^2:100:10^4;
M2= t.^(1);
plot(fig_1,t,100.*M2,'k--','markerfacecolor','white','markersize',5);

t = 10^5:100:10^10;
M3= t.^(1/6);
plot(fig_1,t,10^6.*M3,'k--','markerfacecolor','white','markersize',5);