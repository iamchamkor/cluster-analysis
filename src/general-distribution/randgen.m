function randgen(beta)

a=gen_dist(1.0,50016,0,1.0,beta);

a=a./max(a)

histogram(a,1000);

id=fopen(sprintf('rand_gen_%.2f.dat',beta),'wb');

fwrite(id,a,'float');
 
fclose(id);

id=fopen(sprintf('rand_gen_%.2f.dat',beta),'r');

b=fread(id,[1 Inf],'float');

fclose(id);

end
