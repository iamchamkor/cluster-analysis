function [obj] = loadData(obj,step)

    foo = char(obj.listing(1,step));
    timestring = (foo(strfind(foo,'_t')+2 :1: strfind(foo,'.dat')-1));

    %|particle-position|-----------------------------------------------------------
    obj.positionX = getVector(obj.sim_directory, 'positionX_t', timestring, obj.FLOATTYPE);
    obj.positionX = obj.CELL_LENGTH*obj.positionX;
    obj.positionY = getVector(obj.sim_directory, 'positionY_t', timestring, obj.FLOATTYPE);
    obj.positionY = obj.CELL_LENGTH*obj.positionY;
    obj.positionZ = getVector(obj.sim_directory, 'positionZ_t', timestring, obj.FLOATTYPE);
    obj.positionZ = obj.CELL_LENGTH*obj.positionZ;

    %|particle-velocity|--------------------------------------------------------------
    obj.velocityX = getVector(obj.sim_directory, 'velocityX_t', timestring, obj.FLOATTYPE);
    obj.velocityY = getVector(obj.sim_directory, 'velocityY_t', timestring, obj.FLOATTYPE);
    obj.velocityZ = getVector(obj.sim_directory, 'velocityZ_t', timestring, obj.FLOATTYPE);

    %|particle-energy|--------------------------------------------------------------
    if(obj.energy_switch == 1)
        obj.energy = getVector(obj.sim_directory, 'energy_t', timestring, obj.FLOATTYPE);
    end    
    
    %|particle-density|--------------------------------------------------------------
    if(obj.density_switch == 1)
        obj.density = getVector(obj.sim_directory, 'density_t', timestring, obj.FLOATTYPE);
    end
    
    %|particle-charge|---------------------------------------------------------------
    if(obj.charge_switch == 1)
        obj.charge = getVector(obj.sim_directory, 'charge_t', timestring, obj.FLOATTYPE);
    end
    
    %|particle-force|----------------------------------------------------------------
    if(obj.force_switch == 1)
        obj.forceX = getVector(obj.sim_directory, 'forceX_t', timestring, obj.FLOATTYPE);
        obj.forceY = getVector(obj.sim_directory, 'forceY_t', timestring, obj.FLOATTYPE);
        obj.forceZ = getVector(obj.sim_directory, 'forceZ_t', timestring, obj.FLOATTYPE);
    end
    
    %|particle-dipole-moment|----------------------------------------------------------------
    if(obj.moment_switch == 1)
        obj.momentX = getVector(obj.sim_directory, 'momentX_t', timestring, obj.FLOATTYPE);
        obj.momentY = getVector(obj.sim_directory, 'momentY_t', timestring, obj.FLOATTYPE);
        obj.momentZ = getVector(obj.sim_directory, 'momentZ_t', timestring, obj.FLOATTYPE);
    else
        obj.momentX = zeros(obj.N_PARTICLE,1);
        obj.momentY = zeros(obj.N_PARTICLE,1);
        obj.momentZ = zeros(obj.N_PARTICLE,1);
    end
    
    
end