function [nbrs_cumulative] = neighbours(fig_handle,positionX,positionY,positionZ,pid,nbrs_old,CONTACT_DISTANCE)
    
        dr2 = (positionX-positionX(pid)).^2 +...
                (positionY-positionY(pid)).^2 +...
                    (positionZ-positionZ(pid)).^2;
        

        nbrs = find(dr2<=(CONTACT_DISTANCE^2) & dr2~=0.0);
        
        nbrs_new = mysetdiff(nbrs,nbrs_old);       
        
        nbrs_cumulative = [nbrs_old; nbrs_new];     
        
        if(~isempty(nbrs_new))
             for(n=1:length(nbrs_new))
                 [nbrs_cumulative] = neighbours(fig_handle,positionX,positionY,positionZ,nbrs_new(n),nbrs_cumulative,CONTACT_DISTANCE); 
             end
        end
                   
end