CONTACT_DISTANCE  = DIAMETER;
        
MINIMUM_PARTICLES = 2;
        
        [no_of_clusters, avg_cluster_mass, avg_cluster_size, max_cluster_mass, max_cluster_size, no_of_particles_in_ith_cluster,clusters,cid_maxsize,Rg,Df,T_cls,q_cls] = ...
            getClusterDetails(fig_29,positionX,positionY,positionZ,velocityX,velocityY,velocityZ,charge,CONTACT_DISTANCE,MINIMUM_PARTICLES);
               
        if(MASS_VS_RG)
            p1 = plot(fig_37,no_of_particles_in_ith_cluster,Rg,'or','markersize',5,'markerfacecolor','none');
            [Df_fit,Rg_fit]=fractalDimensionLinearFit(no_of_particles_in_ith_cluster, Rg);
            p2 = plot(fig_37,no_of_particles_in_ith_cluster,Rg_fit,'- b');
            set(p2,'MarkerFaceColor', get(p1,'Color'));
            set(fig_37,'xlim',[1 1000],'ylim',[0.1 100]);            
            x1 = 2;
            y1 = 13;            
            txt1 = sprintf('$t=%.2f, D_f=%.2f$',time, Df_fit);
            leg1=text(fig_37,x1,y1,txt1);
            set(leg1,'interpreter','latex','fontsize',20);
        end
        
        if(SHOW_CLUSTER_MIN_PARTICLES)
            filename   = sprintf('n_min_%d_t_%d',MINIMUM_PARTICLES,time);
            printtitle = sprintf('$N_{cl} > %d,D_{f,avg}=%.2f,t=%d$',MINIMUM_PARTICLES,Df_avg,time);
            title(fig_29,printtitle,'interpreter','latex');
            printpdf(fig_29_gcf,filename);
        end
        

        if(SHOW_MAX_CLUSTER)
        domain(fig_24,clusters{1,cid_maxsize}(clusters{4,cid_maxsize}),...
            clusters{2,cid_maxsize}(clusters{4,cid_maxsize}),...
            clusters{3,cid_maxsize}(clusters{4,cid_maxsize}),'fixed_color','blue','dont_show_axis_lines');            
        
        
        Df_maxsize = Df(cid_maxsize);        
        filename   = sprintf('t_%d',time);
        printtitle = sprintf('$D_f=%.2f, t=%d$',Df_maxsize,time);        
        title(fig_24,printtitle,'interpreter','latex');         
        printpdf(fig_24_gcf,filename);            
        end
        

        if(CLUSTER_HISTOGRAM)                     
            histogram(fig_25, no_of_particles_in_ith_cluster);
            set(fig_25,'xscale','log');
            set(fig_25,'yscale','log');       
            set(fig_25,'xlim',[10 300]);   
            set(fig_25,'ylim',[01 300]);
            
        end
        
        if(CLUSTER_PDF)
            pdf(fig_26,no_of_particles_in_ith_cluster);
            set(fig_26,'xscale','log');
            set(fig_26,'yscale','log'); 
        end

        if(AVG_CLUSTER_TEMP)          
            p = plot(fig_33,time,mean(T_cls),'^b','linewidth',1);  
            Tg_haff    = T_HAFF_YSHIFT*(TAU_INV*time)^(HAFFS_EXPONENT); %+ 3.268*0.01* (TAU_INV*time)^(-11/6);
            gtemp1     = plot(fig_33,time,Tg_haff,'ok','markerfacecolor','k','markersize',5);
            leg        = legend(fig_33,[p,gtemp1],'aggregate temperature','Haff');
            set(leg,'interpreter','latex','box','off'); 

            T_simulation(iter) = mean(T_cls);
            t_simulation(iter) = time;
            
            avg_cluster_mass_simulation(iter) =avg_cluster_mass;
            avg_cluster_size_simulation(iter) =avg_cluster_size;
            max_cluster_mass_simulation(iter) =max_cluster_mass;
            max_cluster_size_simulation(iter) =max_cluster_size;
      
            F = polyfit(log(no_of_particles_in_ith_cluster),log(Rg),1);
            slope     = F(1);
            intercept = F(2);                            
            
            mean_Df(iter)= 1/slope
            lower_Df(iter)= min(Df);
            upper_Df(iter)= max(Df);
            max_cls_Df(iter)=Df(cid_maxsize);
            
        end      
        
        if(PLOT_CLUSTER_NUMBER_DENSITY)
            p = plot(fig_32,time,2*no_of_clusters/70^3,'sr','linewidth',1);
            
            n_simulation(iter) = 2*no_of_clusters/70^3;
            
            avg_no_of_clusters               = N_PARTICLE/avg_cluster_mass;

            p = plot(fig_32,time,avg_no_of_clusters/70^3,'^b','linewidth',1);
            
            n_simulation_mean_field(iter)    = avg_no_of_clusters/70^3;            
        end
        
        if(PLOT_CLUSTER_CHARGE_VAR)
        qq=chargeVsTime(fig_31,q_cls,time);        
        dqq_simulation(iter)= qq;
        end        
        
        
        if(CLUSTER_R)
        R_simulation(iter)= 1.0*qq/(mean(T_cls)*(avg_cluster_size));
        plot(fig_34,time,R_simulation(iter),'sr','linewidth',1);
        end
        
        if(CLUSTER_RADIUS_OF_GYRATION)       
        plot(fig_35,time,mean(Rg),'sr','linewidth',1);
        y= 0.015*time^1.5;
        plot(fig_35,time,y,'+');
        end
        
        if(AVG_FRACTAL_DIM)           
            F = polyfit(log(no_of_particles_in_ith_cluster),log(Rg),1);
            slope     = F(1);
            intercept = F(2); 
            Df_fit    = 1/slope;
            plot(fig_27,time,Df_fit,'^b','linewidth',1);  
            %errorbar(fig_27,time,mean(Df),mean(Df)-min(Df),max(Df)-mean(Df),'^b','linewidth',1);               
            plot(fig_27,time,Df(cid_maxsize),'sr','linewidth',1); 
        end
                
        if(AVG_CLUSTER_SIZE)
            plot(fig_28,time,avg_cluster_size,'o'); 
            y= 0.015*time^1.5;
            plot(fig_28,time,y,'+');
        end
        
        if(FRACTAL_DIMENSION_HISTOGRAM)            
            myhist(fig_30,Df)
        end
        
        if(CHARGE_PDF_NEWBORN_PARTICLES)
            pdf(fig_37,q_cls);
        end

        if(CHARGE_PDF_LARGEST_CLUSTER)
            pdf(fig_38,clusters{8,cid_maxsize}(clusters{4,cid_maxsize}));
        end
        
        if(CHARGE_PDF_ALL_PARTICLES)
            pdf(fig_39,charge);
        end
        
        if(CHARGE_ORDER_PARAMETER)
            q_Order = mean(abs(q_cls));
            plot(fig_cls_01,time,q_Order,'sr','linewidth',1);
        end
        