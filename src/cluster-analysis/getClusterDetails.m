function [no_of_clusters, avg_cluster_mass, avg_cluster_size, max_cluster_mass, max_cluster_size, no_of_particles_in_ith_cluster,clusters,cid_maxsize,Rg,Df,T_cls,q_cls] = ...
    getClusterDetails(fig_handle,positionX,positionY,positionZ,velocityX,velocityY,velocityZ,charge,CONTACT_DISTANCE,MINIMUM_PARTICLES)

positionX_rest = positionX;
positionY_rest = positionY;
positionZ_rest = positionZ;

velocityX_rest = velocityX;
velocityY_rest = velocityY;
velocityZ_rest = velocityZ;

charge_rest    = charge;

no_of_particles_in_ith_cluster  = zeros();
cid                             = 0;
cid_2                           = 0;
tracked_particles               = 0;

cid_maxsize      = 1;
max_cluster_mass = 0;

Rg=zeros();
Df=zeros();
T_agg=zeros();

pos_cls_x=zeros();
pos_cls_y=zeros();
pos_cls_z=zeros();
vel_cls_x=zeros();
vel_cls_y=zeros();
vel_cls_z=zeros();
q_cls=zeros();
m_cls=zeros();

clusters = {};

while( length(positionX_rest)>0 )
    
    i_start = 1;
    
    [nbrs_cumulative] = neighbours(fig_handle,positionX_rest,positionY_rest,positionZ_rest,i_start,[i_start;],CONTACT_DISTANCE);

    cid = cid+1;
    tracked_particles = tracked_particles + length(nbrs_cumulative);
    
    
    if(length(nbrs_cumulative)>=MINIMUM_PARTICLES)        
        
        cid_2 = cid_2+1;      
        
        no_of_particles_in_ith_cluster(cid_2) = length(nbrs_cumulative);
        
%         domain(fig_handle,positionX_rest(nbrs_cumulative),...
%                              positionY_rest(nbrs_cumulative),...
%                                 positionZ_rest(nbrs_cumulative),'random_color','blue','show_axis_lines');
                            
        clusters{1,cid_2} = positionX_rest;
        clusters{2,cid_2} = positionY_rest;
        clusters{3,cid_2} = positionZ_rest;
        
        clusters{4,cid_2} = nbrs_cumulative;       
        
        clusters{5,cid_2} = velocityX_rest;
        clusters{6,cid_2} = velocityY_rest;
        clusters{7,cid_2} = velocityZ_rest;
        
        clusters{8,cid_2} = charge_rest;
        
        
        [Rg(cid_2),Df(cid_2)] = fractal(clusters,cid_2);  
        
        [T_agg(cid_2)] = temperature2(clusters, cid_2);
        
        [pos_cls_x(cid_2),pos_cls_y(cid_2),pos_cls_z(cid_2),...
            vel_cls_x(cid_2),vel_cls_y(cid_2),vel_cls_z(cid_2),q_cls(cid_2),m_cls(cid_2)] = cluster2particle(clusters, cid_2);
        
        if(length(nbrs_cumulative)>max_cluster_mass)            
            cid_maxsize      = cid_2;               
            max_cluster_mass = length(nbrs_cumulative);
        end
                                
    end
        
    %length(nbrs_cumulative)
    %tracked_particles
    
    positionX_rest(nbrs_cumulative) = [];
    positionY_rest(nbrs_cumulative) = [];
    positionZ_rest(nbrs_cumulative) = [];
    
    velocityX_rest(nbrs_cumulative) = [];
    velocityY_rest(nbrs_cumulative) = [];
    velocityZ_rest(nbrs_cumulative) = [];
    
    charge_rest(nbrs_cumulative) = [];
    
end

no_of_clusters   = cid_2;
max_cluster_size = max_cluster_mass^(1/2.5);
avg_cluster_mass = avgclustermass(no_of_particles_in_ith_cluster);
avg_cluster_size = avg_cluster_mass^(1/2.5);

[T_cls] = granularTemperaturePreciseCluster2Particle(vel_cls_x,vel_cls_y,vel_cls_z,pos_cls_x,pos_cls_y,pos_cls_z,q_cls,m_cls);

%          domain(fig_handle,pos_cls_x,...
%                               pos_cls_y,...
%                                  pos_cls_z,'random_color','blue','show_axis_lines');

end
