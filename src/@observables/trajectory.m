function [] = trajectory(observables, data, fig_handle, pid, step)
    
    VIEW_ANGLE  = data.VIEW_ANGLE;
    CELL_LENGTH = data.CELL_LENGTH;

    if(fig_handle~=0)
        traj_X(step) = data.positionX(pid);
        traj_Y(step) = data.positionY(pid);
        traj_Z(step) = data.positionZ(pid);
        plot3(fig_handle,traj_X,traj_Y,traj_Z,'o');
        box(fig_handle,'on'); fig_handle.BoxStyle = 'full';
        set(fig_handle,'View',[-VIEW_ANGLE,VIEW_ANGLE]);
        axis(fig_handle,[-CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2]);
    end
    
end