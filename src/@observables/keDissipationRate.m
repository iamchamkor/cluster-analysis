function [ke] = keDissipationRate(observables, data, fig_handle, time_vector, ke_vector)
   
    dt   = data.DELTA_T*data.ITER_DATA_GEN;
    dedt = -gradient(ke_vector,dt);
    plot(fig_handle,time_vector,dedt,'-b','linewidth',2);
    %set(fig_7,'ylim',[0 0.016]);
    
end