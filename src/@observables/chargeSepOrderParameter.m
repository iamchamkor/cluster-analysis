function [leg_3,Tg_haff,Tg_sim] = chargeSepOrderParameter(observables, data, fig_handle, time)
    
        N_PARTICLE  = data.N_PARTICLE;
        CELL_LENGTH = data.CELL_LENGTH;
        VIEW_ANGLE  = data.VIEW_ANGLE;
    
        nboxes=12;      
        
        np_box=zeros(nboxes+1,nboxes+1,nboxes+1);

        charge_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        charge_box_k_space=zeros(nboxes+1,nboxes+1,nboxes+1);
       
        for(pid=1:N_PARTICLE)
            ibox=floor( (data.positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (data.positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (data.positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                np_box(ibox+1,jbox+1,kbox+1)        = np_box(ibox+1,jbox+1,kbox+1)        + 1;
                charge_box(ibox+1,jbox+1,kbox+1)    = charge_box(ibox+1,jbox+1,kbox+1)   + data.charge(pid);
            end
        end
        
        com_charge_box = charge_box./np_box;
        com_charge_box(isnan(com_charge_box)) = 0;
             
        dx = CELL_LENGTH/nboxes;
                            
        [X,Y,Z] = meshgrid(-CELL_LENGTH/2+0.5*dx:dx:CELL_LENGTH/2-0.5*dx,...
                            -CELL_LENGTH/2+0.5*dx:dx:CELL_LENGTH/2-0.5*dx,...
                             -CELL_LENGTH/2+0.5*dx:dx:CELL_LENGTH/2-0.5*dx);
                         
                         for i=1:length(X)
                             for j=1:length(Y)
                                 for k=1:length(Z)
                                     
                                     charge_box_k_space(i,j,k) = charge_box_k_space(i,j,k) +  com_charge_box(i,j,k) * cos( (2*pi/nboxes)*(i*X(i)+j*(Y(j)+k*Z(k))) );
                                                                          
                                 end
                             end
                         end
                

                         contourf(charge_box_k_space(:,:,2));

        % plot(time,cav,'ob');hold on;
    
end