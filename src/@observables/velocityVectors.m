function [] = velocityVectors(observables, data, fig_handle)

        VIEW_ANGLE  = data.VIEW_ANGLE;    
        CELL_LENGTH = data.CELL_LENGTH;
    
        nboxes=31;      
        
        np_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        density_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumX_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumY_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumZ_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        
        for(pid=1:data.N_PARTICLE)
            ibox=floor( (data.positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (data.positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (data.positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                np_box(ibox+1,jbox+1,kbox+1)        = np_box(ibox+1,jbox+1,kbox+1)        + 1;
                density_box(ibox+1,jbox+1,kbox+1)   = density_box(ibox+1,jbox+1,kbox+1)   + data.density(pid);
                momentumX_box(ibox+1,jbox+1,kbox+1) = momentumX_box(ibox+1,jbox+1,kbox+1) + data.velocityX(pid);
                momentumY_box(ibox+1,jbox+1,kbox+1) = momentumY_box(ibox+1,jbox+1,kbox+1) + data.velocityY(pid);
                momentumZ_box(ibox+1,jbox+1,kbox+1) = momentumZ_box(ibox+1,jbox+1,kbox+1) + data.velocityZ(pid);
            end
        end
        
        com_density_box   = density_box  ./np_box;
        com_momentumX_box = momentumX_box./np_box;
        com_momentumY_box = momentumY_box./np_box;
        com_momentumZ_box = momentumZ_box./np_box;      
        
        com_density_box(isnan(com_density_box))    =0;
        com_momentumX_box(isnan(com_momentumX_box))=0;
        com_momentumY_box(isnan(com_momentumY_box))=0;
        com_momentumZ_box(isnan(com_momentumZ_box))=0;        
       
    if(fig_handle~=0)
        
        nboxes_allocated = nboxes+1;                      
        
        [X,Y,Z] = meshgrid(-CELL_LENGTH/2:CELL_LENGTH/nboxes:CELL_LENGTH/2,...
                            -CELL_LENGTH/2:CELL_LENGTH/nboxes:CELL_LENGTH/2,...
                             -CELL_LENGTH/2:CELL_LENGTH/nboxes:CELL_LENGTH/2);
        
        X = X + 0.5*(CELL_LENGTH/nboxes);
        Y = Y + 0.5*(CELL_LENGTH/nboxes);
        Z = Z + 0.5*(CELL_LENGTH/nboxes);
                         
        quiver3(fig_handle,X,Y,Z,com_momentumX_box,com_momentumY_box,com_momentumZ_box); 
        set(fig_handle,'View',[-VIEW_ANGLE,VIEW_ANGLE]);
        axis(fig_handle,[-CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2]);
        
                      
end