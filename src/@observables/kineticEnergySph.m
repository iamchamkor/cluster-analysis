function [kinetic_energy] = kineticEnergySph(observables, data, fig_handle, time)
    
    N_PARTICLE  = data.N_PARTICLE;
    CELL_LENGTH = data.CELL_LENGTH;    

if(fig_handle~=0)
    
        nboxes=data.N_BOXES_X;
        
        np_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        
        momentumX_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumY_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumZ_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        
        fluctuating_velocityX = zeros(N_PARTICLE,1);
        fluctuating_velocityY = zeros(N_PARTICLE,1);
        fluctuating_velocityZ = zeros(N_PARTICLE,1);

        for(pid=1:N_PARTICLE)          
            ibox=floor( (data.positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (data.positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (data.positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes);
                fprintf('problem in binning!')
            else
                
                np_box(ibox+1,jbox+1,kbox+1) = np_box(ibox+1,jbox+1,kbox+1)+1;
                
                momentumX_box(ibox+1,jbox+1,kbox+1) = momentumX_box(ibox+1,jbox+1,kbox+1) + 1*data.velocityX(pid);
                momentumY_box(ibox+1,jbox+1,kbox+1) = momentumY_box(ibox+1,jbox+1,kbox+1) + 1*data.velocityY(pid);
                momentumZ_box(ibox+1,jbox+1,kbox+1) = momentumZ_box(ibox+1,jbox+1,kbox+1) + 1*data.velocityZ(pid);
                
            end
        end
        
        com_velocityX_box = momentumX_box./np_box;
        com_velocityY_box = momentumY_box./np_box;
        com_velocityZ_box = momentumZ_box./np_box;

        com_velocityX_box(isnan(com_velocityX_box))=0;
        com_velocityY_box(isnan(com_velocityY_box))=0;
        com_velocityZ_box(isnan(com_velocityZ_box))=0;
        
        for(pid=1:N_PARTICLE)
            ibox=floor( (data.positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (data.positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (data.positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                fluctuating_velocityX(pid) = data.velocityX(pid) - com_velocityX_box(ibox+1,jbox+1,kbox+1);
                fluctuating_velocityY(pid) = data.velocityY(pid) - com_velocityY_box(ibox+1,jbox+1,kbox+1);
                fluctuating_velocityZ(pid) = data.velocityZ(pid) - com_velocityZ_box(ibox+1,jbox+1,kbox+1);                                
            end
        end
        

    HAFFS_EXPONENT = data.HAFFS_EXPONENT;
    TAU_INV        = data.TAU_INV;
    T_HAFF_YSHIFT  = data.T_HAFF_YSHIFT;

    if(fig_handle~=0)
        v_abs_2    = (data.velocityX.^2 + data.velocityY.^2 + data.velocityZ.^2);
        v_abs_2    = sum(v_abs_2)/N_PARTICLE;
        ke         = (1/2)*1.0*v_abs_2;        
        Tg_sim     = (2/3)*ke;
        %Tg_haff    = T_HAFF_YSHIFT*(TAU_INV*time)^(HAFFS_EXPONENT); %+ 3.268*0.01* (TAU_INV*time)^(-11/6);
        %gtemp1     = plot(fig_handle,time,Tg_haff,'ok','markerfacecolor','k','markersize',5);
        gtemp2     = plot(fig_handle,time,Tg_sim,'*b');
        
        kinetic_energy = ke;
        
%         v_abs_2    = (fluctuating_velocityX.^2 + fluctuating_velocityY.^2 + fluctuating_velocityZ.^2);
%         v_abs_2    = sum(v_abs_2)/N_PARTICLE;
%         ke         = (1/2)*1.0*v_abs_2;        
%         Tg_sim     = (2/3)*ke;
%         gtemp3     = plot(fig_handle,time,Tg_sim,'ob');
        
        %leg_3      = legend(fig_handle,[gtemp1,gtemp2,gtemp3],'Haff','Simulation $\frac{1}{2}m\langle v_i^2 \rangle$','Simulation $\frac{1}{2}m\langle(v_i - V)^2\rangle$');
        %set(leg_3,'interpreter','latex','box','off');
        
        %observables.T_monomers = Tg_sim;
        
    end
        
end

end