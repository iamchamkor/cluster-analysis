function [s] = domain(observables, data, fig_handle, color_condition,color_vector,axis_condition,time)

   CELL_LENGTH   = data.CELL_LENGTH;
   VIEW_ANGLE    = data.VIEW_ANGLE;
   PARTICLE_SKIP = data.PARTICLE_SKIP;
   DOT_SIZE      = 10;

   color = color_vector(1:PARTICLE_SKIP:end);
   
    if(fig_handle~=0)

        if(strcmp(color_condition,'fixed_color'))
        s = scatter3(fig_handle,data.positionX(1:PARTICLE_SKIP:end),data.positionY(1:PARTICLE_SKIP:end),...
            data.positionZ(1:PARTICLE_SKIP:end),DOT_SIZE,...
            color,'filled');
        fprintf('particle-skip=%d!',PARTICLE_SKIP);        
        
        elseif(strcmp(color_condition,'random_color'))
        s = scatter3(fig_handle,data.positionX(1:PARTICLE_SKIP:end),data.positionY(1:PARTICLE_SKIP:end),...
            data.positionZ(1:PARTICLE_SKIP:end),DOT_SIZE,...
            'filled');
        else
        printf('particles in box: problem in color!');
        end

        set(fig_handle,'XTick',[]);
        set(fig_handle,'YTick',[]);
        set(fig_handle,'ZTick',[]);
        
        if(~strcmp(axis_condition,'show_axis_lines'))
        fig_handle.XAxis.Color='w';
        fig_handle.YAxis.Color='w';
        fig_handle.ZAxis.Color='w';
        end
        
        if(data.charge_switch==1)
            FONT_SIZE=20;
            cbar = colorbar(fig_handle);
            colormap(fig_handle,brewermap([],'RdYlGn'));
            colormap(cbar,brewermap([],'RdYlGn'));
            ylabel(cbar,sprintf('$q$(t=%d)',time),'fontsize',FONT_SIZE,'interpreter','latex');
            set(cbar,'fontsize',FONT_SIZE,'fontweight','normal','FontName','times');   
            %caxis(fig_handle,[-3 3]);
        end

        box(fig_handle,'on'); 
        
        if(~strcmp(axis_condition,'show_axis_lines'))
            fig_handle.BoxStyle = 'back';
        else
            fig_handle.BoxStyle = 'full';
        end
        
        set(fig_handle,'View',[-VIEW_ANGLE,VIEW_ANGLE]);
        axis(fig_handle,[-CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2]);
    else
        s   = 0;
    end
    
end