function [] = observablesMain(observables, obj_data, time, step)

    %max(obj_data.charge)
    %min(obj_data.charge)

    if(observables.Q_FLUC_ONE_PARTICLE)
        observables.chargeFluctuationsOnSingleParticle(obj_data, observables.fig_39, time); 
    end

    if(observables.CHARGE_SEP_ORDER_PARA)   
        observables.chargeSepOrderParameter(obj_data, observables.fig_38, time);
    end

    if(observables.VELOCITY_VECTORS)   
        observables.velocityVectors(obj_data, fig_0);
    end
    
    if(observables.MIDPLANE_VELOCITY_SPH)
        observables.midPlaneVelocity(obj_data, observables.fig_37, time, step);
    end
    
    if(observables.MIDPLANE_DENSITY_SPH)
        observables.midPlaneDensity(obj_data, fig_36, time);
    end

    if(observables.PARTICLES_IN_BOX)        
        [s] = observables.domain(obj_data, observables.fig_1, 'fixed_color', obj_data.charge, 'show_axis_lines', time);
        if(obj_data.MOVIE)
            makeMovie(observables.fig_1_gcf,obj_data.video_name);
        end  
    end
   
    if(observables.PLOT_TEMPERATURE)                     
        [leg_3,Tg_haff,Tg_sim] = observables.granularTemperaturePrecise(obj_data, observables.fig_3, time);
        %[leg_3,Tg_haff,ke]= granularTemperature(fig_3,time,obj_data.velocityX,obj_data.velocityY,obj_data.velocityZ);
        %[leg_3,Tg_haff,Tg_sim] = granularTemperaturePrecise(fig_3,time,obj_data.velocityX,obj_data.velocityY,obj_data.velocityZ,obj_data.positionX,obj_data.positionY,obj_data.positionZ);                      
    end

    if(observables.PLOT_KE_SPH)                     
         [ke]=observables.kineticEnergySph(obj_data, observables.fig_40, time);
        %[leg_3,Tg_haff,ke]= granularTemperature(fig_3,time,obj_data.velocityX,obj_data.velocityY,obj_data.velocityZ);
        %[leg_3,Tg_haff,Tg_sim] = granularTemperaturePrecise(fig_3,time,obj_data.velocityX,obj_data.velocityY,obj_data.velocityZ,obj_data.positionX,obj_data.positionY,obj_data.positionZ);                      
    end    
              
    if(observables.PLOT_DISSIPATION)
        observables.ke_sph(step) = ke;
        observables.t_sph(step)  = time;
    end    
    
    if(observables.PLOT_FLUCTUATIONS)                
        [observables.Ma] = observables.fluctuations(obj_data, observables.fig_4, time);    
    end   
    
    if(observables.SINGLE_PARTICLE_IN_BOX)        
        observables.trajectory(obj_data, fig_13, 10000, step)
    end
    
    if(observables.PLOT_CHARGE_VS_TIME)
        [q] = chargeVsTime(observables.fig_6,obj_data.charge,time);
    end
    
    if(observables.PLOT_MSD)
        if step==1
            X_0 = obj_data.positionX; Y_0 = obj_data.positionY; Z_0 = obj_data.positionZ;
        end
        [dr_msd,t_msd] = observables.msd(obj_data, observables.fig_16, X_0, Y_0, Z_0, step, time);
        %[dr_msd,t_msd] = msd(fig_16,obj_data.positionX,obj_data.positionY,obj_data.positionZ,X_0,Y_0,Z_0,step,time);
    end
    
    %--------------------------------------------------------------------------
    if(observables.PLOT_V_DIST)
        pdf(observables.fig_9,obj_data.velocityX);
    end
    
    %--------------------------------------------------------------------------
    if(observables.PLOT_CHARGE_DIST)
        [observables.pdf_charge_monomers{1}, observables.pdf_charge_monomers{2}] = pdf(observables.fig_10,obj_data.charge);
        observables.var_charge_monomers = var(obj_data.charge);
    end
    %--------------------------------------------------------------------------
    
    if(observables.PLOT_F_DIST)
        pdf(fig_8,obj_data.forceX);
    end
    
    %--------------------------------------------------------------------------
    if(observables.PLOT_VAR_DIPOLE_MOMENT)
     observables.var_mom_mon = var(obj_data.momentX)+var(obj_data.momentY)+var(obj_data.momentZ);
     plot(observables.fig_42,time,observables.var_mom_mon,'or','markersize',10);
    end
    
    %--------------------------------------------------------------------------
%     if(observables.PLOT_DISSIPATION_RATE)
%         keDissipationRate(data, fig_7, t_sim_v2, ke_sim_v2);    
%     end
    
end