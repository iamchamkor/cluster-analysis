%function [dr_msd,t_msd] = msd(fig_handle,positionX,positionY,positionZ,X_0,Y_0,Z_0,step,time)
function [dr_msd,t_msd] = msd(observables, data, fig_handle, X_0, Y_0, Z_0, step, time)    

        dx = (data.positionX - X_0);
        dy = (data.positionY - Y_0);
        dz = (data.positionZ - Z_0);
        
        ds2 = (dx.^2 + dy.^2 + dz.^2);
        
        MSD = sum(ds2)/data.N_PARTICLE;
        
        plot(fig_handle,time,MSD,'.r','markersize',10);
        
        dr_msd(step) = MSD;
        t_msd(step) = time;
        
end