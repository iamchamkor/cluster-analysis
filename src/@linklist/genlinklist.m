function [head,next] = genlinklist(linklist, data)

    nboxes = data.N_BOXES_X;
    head   = zeros(data.N_BOXES_X^3,1);
    next   = zeros(data.N_PARTICLE,1);
    
    head(:) = -1;

    %Vector cell index to which this atom belongs    
    ibox=floor( (data.positionX./data.CELL_LENGTH + 0.5).*nboxes);
    jbox=floor( (data.positionY./data.CELL_LENGTH + 0.5).*nboxes);
    kbox=floor( (data.positionZ./data.CELL_LENGTH + 0.5).*nboxes);
        
    %Translate the vector cell index, mc, to a scalar cell index
    boxindex=(ibox+jbox.*nboxes+kbox.*nboxes.*nboxes) + 1; % +1 due to matlab indexing, starts from 1
    
    for(pid=1:data.N_PARTICLE)   

        %Link to the previous occupant (or EMPTY if you're the 1st), but name it "next" which is convenient while unrolling the list
        next(pid) = head(boxindex(pid));

        %The last one goes to the header, the list starts from the head while unrolling the list
        head(boxindex(pid)) = pid;

    end
    
end