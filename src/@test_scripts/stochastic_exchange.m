function [obj] = stochastic_exchange(obj)

format long;

figure; fig_1 = axes; fig_1_gcf = gcf; hold(fig_1,'on'); box(fig_1,'on');
%figure; fig_2 = axes; fig_2_gcf = gcf; hold(fig_2,'on'); box(fig_2,'on');

set(get(fig_1,'xlabel'),'string','$E_{\mathrm{kin}}\;(J)$','fontsize', 21,...
    'FontWeight', 'normal', 'FontName', 'Times','Interpreter', 'Latex','rot',00);
set(get(fig_1,'ylabel'),'string','$Z$','fontsize', 21,...
    'FontWeight', 'normal', 'FontName', 'Times','Interpreter', 'Latex','rot',90);
set(fig_1,'fontsize',21,'fontname','times');

set(fig_1,'xscale','log');
set(fig_1,'yscale','log');

KE = logspace(-15,-12,400);

KE = KE;

C = 1.0e15*(1e-1 + (10^0.5 - 1e-1)*rand(1,400));

maxC = max(C)
minC = min(C)

k = 0.83+0.1*0.83*(rand(1,400)-0.5);

maxK = max(k)
minK = min(k)

size(KE)
size(C)

C_blum = 10^15;
Z = ( (C_blum*KE).^0.83 );
Z2    = round( (C.*KE).^k );

p1 = plot(fig_1,KE,Z,'-k');    
p2 = plot(fig_1,KE,Z2,'+b');


xlim(fig_1,[1e-15 1e-12]);
ylim(fig_1,[1 1e3]);

pause(1.0);
leg=legend(fig_1,[p1,p2],'Poppe et al., 2000','Present stochastic model');
set(leg,'box','off','location','northwest','interpreter','latex','fontsize',14.5);
pause(1.0);
set(fig_1_gcf,'PaperUnits','centimeters','PaperPosition',[2 2 20 19])
pause(1.0);
print(fig_1_gcf,'stochastic-exchange-poppe','-dpdf');
pause(1.0);

end
