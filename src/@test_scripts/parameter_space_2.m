function [] = parameter_space_2(obj)

close all;
set(0,'DefaultFigureVisible','on');
cd '/scratch.local/data/singh/dev/matbox/out/Nc_2';

cd 'phi_0p076_K_1p0';

figure;figure;figure;
[fig_1,fig_1_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlog',18);

load pdf_charge_clusters;
load t;
y=pdf_charge_clusters;

time_iter = 1;
time=t(time_iter)
y_gauss  = normpdf(y{time_iter,1});
p1=plot(fig_1,y{time_iter,1},y_gauss,'-k','linewidth',2);
p2=plot(fig_1,y{time_iter,1},y{time_iter,2},'.b','linewidth',1.0,'markersize',10);

time_iter = 15;
time=t(time_iter)
p3=plot(fig_1,y{time_iter,1},y{time_iter,2},'.r','linewidth',1.0,'markersize',10);

time_iter = 20;
time=t(time_iter)
p4=plot(fig_1,y{time_iter,1},y{time_iter,2},'.c','linewidth',1.0,'markersize',10);

leg = legend(fig_1,[p2,p3,p4],'$t=5$','28','836');    
set(leg,'interpreter','latex','box','off','fontsize',14,'location','northwest');

set(fig_1,'xlim',[-8 8]);
set(fig_1,'ylim',[10^-5 10^1]);

xlabel(fig_1,'$\tilde{q}$');
ylabel(fig_1,'$f(\tilde{q})$');

txt=text(fig_1,1.5,0.8,'$Clusters$');
set(txt,'interpreter','latex','fontsize',15);

end