function [obj] = check_plot(obj)

close all;
set(0,'DefaultFigureVisible','on');

load t;
load avg_cluster_size;

figure(1);
plot(t,avg_cluster_size,'o'); hold on;

tt = 100:10:5000;
plot(tt,0.1.*tt.^0.5,'-');

set(gca,'xscale','log');
set(gca,'yscale','log');

figure(2);
load Df_fit;
plot(t,Df_fit,'o');
set(gca,'ylim',[0 3]);

figure(3);
load T_clusters;
plot(t,T_clusters,'o');
hold on;
tt = 1:10:1000;
plot(tt,0.1.*tt.^(-5/3),'-');

set(gca,'xscale','log');
set(gca,'yscale','log');

end