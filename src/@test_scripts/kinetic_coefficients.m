function [] = kinetic_coefficients(obj)

close all;
set(0,'DefaultFigureVisible','on');



R = 0:0.001:1; %or K or R in matlab and notebook.

l1 = sqrt(1-R.^2)
l  = sqrt(4-R.^2)


%n
a_n_1 = ...
+ ( -R.^2 .* l.^3  .* atan(l1./R) )...
+ ( l1.*R.*l.*(-4+7.*R.^2)+l1.*pi.*(-1+R.^2).*(8-4.*l+R.^2 .*(-6+l)) )...
+ ( l1.*4.*(4-7.*R.^2+3.*R.^4).* atan(R./l) )...
+ ( -R.^2 .* l.^3 .* atan(l1./R) )...
+ ( l1.*l.* (R.*(-4+7.*R.^2)+pi.*(4-5.*R.^2+R.^4)) )...
+ ( -4.*l1.*(4-7.*R.^2 +3.*R.^4) .* atan(l./R) )

plot(R , a_n_1 ./ (l1.^3 .* l.^3) )

%T
% a_T_1 = R.*(8-5.*R^2).*l + pi.*(16-10.*R^2+3.*R^4) 
% 
% a_T_2 = (-32+20.*R^2-6.*R^4) .* atan(R./l)
% 
% a_T_3 = l.*(2.*pi.*(-4+R^2)^2 + R.*(-32+28.*R^2 + R^4) )
% 
% a_T_4 = -4.*(32-20.*R^2+9.*R^4) .* acot(R./l)
% 
% a_T_5 = R.*l.*(-32+28.*R^2 +R^4) + 2.*pi.*(R^2 .* (20-8.*l) + R^4 .* (-9+l) + 16.*(-2+l) ) 
%  
% a_T_6 = 4.*(32-20.*R^2 +9.*R^4) .* atan(R./l)
%                                        
%   
% %qq
% a_q_1 = 2.*R.*l.*(-1+R^2)-pi.*(4-2.*R^2+R^4)
% 
% a_q_2 = 2.*(4-2.*R^2+R^4) .* atan(R./l)
% 
% a_q_3 = 16.*l.*sqrt(pi-pi.*R^2).*(4-5.*R^2+R^4)^2
% 
% a_q_4 = -sqrt(pi).*R.*l.*( pi.*(-4+R^2).*(1+2.*R^2)+2.*R.*l1.*(-8-54.*R^2+33.*R^4+2.*R^6) )
% 
% a_q_5 = 2.*sqrt(pi).*R.*l^5.*(1+2.*R^2) .* atan(R./l1)
% 
% a_q_6 = 16.*sqrt(pi).*R.*l1^5.*(4+5.*R^2) .* atan(l./R)
% 
% a_q_7 = 8.*l.*sqrt(pi-pi.*R^2).*(4-5.*R^2+R^4)^2
% 
% a_q_8 = -sqrt(pi).*R.*l^5.*(1+2.*R^2) .* atan(l1./R)
% 
% a_q_9 = sqrt(pi).*R.*l1.*R.*l.*(8+54.*R^2-33.*R^4-2.*R^6)
% 
% a_q_10 = 8.*sqrt(pi).*R .* (-1+R^2)^2 .* (4+5.*R^2) .* atan(l./R)
                  
                      


end