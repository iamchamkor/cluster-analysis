function [] = parameter_space(obj)

close all;
set(0,'DefaultFigureVisible','on');
cd '/scratch.local/data/singh/dev/matbox/out/Nc_3';

phi_s=["0p002", "0p0036", "0p007", "0p013", "0p030", "0p076", "0p137",  "0p213"];
k_s  =["0p001", "0p0036", "0p01",  "0p023", "0p0625", "0p125", "0p4",   "1p0", "5p0"];

phi=[0.002 0.0036 0.007 0.013 0.030 0.076 0.137 0.213];
k  =[0.001 0.0036 0.01 0.023 0.0625 0.125 0.4 1.0 5.0];

[PHI,K]=meshgrid(phi,k);

[fig_1,fig_1_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
[fig_2,fig_2_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
% [fig_3,fig_3_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
[fig_4,fig_4_gcf]  = createFig(1,         '$tv_\textrm{ref}/d$',    '$\langle D_f \rangle$', 'holdon',   'boxon','linlin',11);

li=0;
l1_handle={};
l1_name={};
Df_all = {};
t_all  = {};

markers = {'o','s','d','.','x','s','d','^','v','>','<','p','h'};
colors  = {[0.3010, 0.7450, 0.9330], [1, 0.2, 0.6], [0, 0, 1.0]}

i_start = 4;
i_end   = 6;

j_start = 9;
j_end   = 9;

for i=i_start:i_end
for j=j_start:j_end

        
        li=li+1;
        
        outdir = sprintf('phi_%s_K_%s',phi_s(i),k_s(j));    
        if exist(outdir,'dir')
            cd(outdir);
            pwd            
            
            load t;
            
            color_grad = 1 - (i-i_start)/(i_end-i_start);
            
            R = rand(1)
            G = rand(1)
            B = color_grad
            
            color = [R,G,B];
            
            %|T|-----------------------------------------------------------            
            load avg_cluster_size;
            y = avg_cluster_size;
                        
            l1_handle{li} = plot(fig_1,t,y,'o','color',color,'markersize',8, 'linewidth', 1.5); 
            l1_name{li}   = sprintf('$\\phi=%.3f$',phi(i));
                        
%             set(fig_1,'xlim',[10^-2 10^5]);
%             set(fig_1,'ylim',[10^-6 10^1]);
            set(fig_1,'xscale','log');
            set(fig_1,'yscale','log');
            
            %|<dq^2>|-----------------------------------------------------------
            load pdf_charge_clusters;
            y = pdf_charge_clusters;
            
            t(20)
            plot(fig_2,y{20,:},y{20,:},markers{li},'color',color,'markersize',5, 'linewidth', 1.5); 
            l2_name{li}   = sprintf('$\\phi=%.3f$',phi(i));
            set(fig_2,'xlim',[-7 +7]);
            set(fig_2,'ylim',[10^-5 1]);
            set(fig_2,'xscale','lin');
            set(fig_2,'yscale','log');
            
            %|n|-----------------------------------------------------------            
%             load no_of_clusters;
%             y = no_of_clusters./70^3;
% 
%             plot(fig_3,t,y,'o','color',color,'markersize',8, 'linewidth', 1.5); 
%             l3_name{li}   = sprintf('$\\phi=%.3f$',phi(i));
%             set(fig_3,'xlim',[1 10^4]);
%             %set(fig_3,'ylim',[0.001 10^4]);
%             set(fig_3,'xscale','log');
%             set(fig_3,'yscale','log');       
            
            %|K|-----------------------------------------------------------            
            load Df_fit;
            y = Df_fit;

            t_all{li}  = t;           
            Df_all{li} = y;
                
            
            plot(fig_4,t,y,markers{li},'color',colors{li},'markerfacecolor',colors{li},'markersize',4, 'linewidth', 1.5); 
            l4_name{li}   = sprintf('$\\phi=%.3f$',phi(i));
            set(fig_4,'xlim',[300 2500]);
            set(fig_4,'ylim',[0.5 3]);
            set(fig_4,'xscale','log');
            set(fig_4,'yscale','lin');                                  

            cd('..');
        end
        
    end
end


%plot(fig_4,t, Df,'s','color',color,'markerfacecolor',color,'markersize',8, 'linewidth', 1.5);

tt     = 1:1:10000;
Df_2p0 = []; Df_2p0(1:10000) = 1.94;
Df_2p5 = []; Df_2p5(1:10000) = 2.46;

plot(fig_4,tt, Df_2p0,'--','color',[0.75, 0.75, 0],'markerfacecolor',color,'markersize',8, 'linewidth', 1.0);
plot(fig_4,tt, Df_2p5,'--','color',[0.75, 0.75, 0],'markerfacecolor',color,'markersize',8, 'linewidth', 1.0);

xticks([500 1000 2000])
xticklabels({'500','1000','2000'})

txt=text(fig_4,330,2.55,'DLPCA');
set(txt,'interpreter','latex','fontsize',11);

txt=text(fig_4,1700,1.830,'BCCA');
set(txt,'interpreter','latex','fontsize',11);
%|legends|------------------


tt = 100:1:3000;
plot(fig_1,tt,0.015.*1.*tt.^(3/2),'-k','linewidth',2);
txt=text(fig_1,8,0.00015,'$-5/3$');
set(txt,'interpreter','latex','fontsize',18);


% tt = 80:1:4000;
% plot(fig_2,tt,0.0005.*10^2.*tt.^(-1),'-k','linewidth',2);
% txt=text(fig_2,100,0.00005,'$-1$');
% set(txt,'interpreter','latex','fontsize',18);

leg = legend(fig_4,l4_name);    
set(leg,'interpreter','latex','box','off','fontsize',11,'location','south');


end