function [obj] = lees_edward_check(obj)

CELL_LENGTH = 2*pi;
SHEAR_RATE  = 1.0/CELL_LENGTH;

vx = 0.9;
z  = 0.501;

cz = round(z)

if(cz~=0)
    
   dz = z - 0.5*round(z);
   vx = ( -0.5*sign(dz) + dz )*SHEAR_RATE*CELL_LENGTH;
   
end

z1 = +0.48; vx1 = +0.49;
z2 = -0.48; vx2 = -0.49;

    
dz = z1-z2;
cz = round(dz);
dz = dz - cz;

if(cz~=0)
   
    dvx = vx1 - (-vx2 - dz*SHEAR_RATE*CELL_LENGTH)
    
end

end



