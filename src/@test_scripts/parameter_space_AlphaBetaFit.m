function [] = parameter_space_2(obj)

close all;
set(0,'DefaultFigureVisible','on');
cd '/scratch.local/data/singh/dev/matbox/out/Nc_2';

cd 'phi_0p137_K_1p0';

figure;
[fig_1,fig_1_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','loglog',18);
[fig_2,fig_2_gcf]  = createFig(1,         '$\tilde{m}$',    '$\tilde{q}$', 'holdon',   'boxon','linlin',18);
[fig_3,fig_3_gcf]  = createFig(1,         '$\tilde{m}$',    '$\tilde{v}$', 'holdon',   'boxon','linlin',18);

load t;
load Alpha_fit;
load Beta_fit;

load m;
load q_cls_abs;
load v_cls;

time_iter = 5;
time = t(time_iter)

m = m{time_iter};           m=m./std(m);
q = q_cls_abs{time_iter};   q=q./std(q);
v = v_cls{time_iter};       v=v./std(v);



p1=plot(fig_1,t,Alpha_fit,'or','linewidth',1.0,'markersize',10);
p2=plot(fig_1,t,Beta_fit,'^b','linewidth',1.0,'markersize',10);
leg = legend(fig_1,[p1,p2],'$\alpha$','$\beta$');    
set(leg,'interpreter','latex','box','off','fontsize',18,'location','southwest');

p2=plot(fig_2,m,q,'sr','linewidth',1.0,'markersize',10);

p3=plot(fig_3,m,v,'sr','linewidth',1.0,'markersize',10);


m = 1:0.1:20;

a = max(q)/(1-1/max(m));
c = max(q) - a;
q = a./m + c;

a = max(v)/(1-1/max(m));
c = max(v) - a;
v = a./m + c;

plot(fig_2,m,q,'-k','linewidth',2.0);
% set(fig_2,'xlim',[0 20]);
% set(fig_2,'ylim',[0 8]);

txt=text(fig_2,6,4,'$\tilde{q}\propto\tilde{m}^{-1}$');
set(txt,'interpreter','latex','fontsize',18);

plot(fig_3,m,v,'-k','linewidth',2.0);
% set(fig_3,'xlim',[0 20]);
% set(fig_3,'ylim',[0 8]);



end