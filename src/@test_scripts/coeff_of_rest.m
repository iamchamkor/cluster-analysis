function [obj] = coeff_of_rest(obj)

clear all;
close all;
clc;

setup;

simulation_output_directory      =('3D_N50016_L70.0_CCR0.85_PHI0.076_EC277.21_DC27.71_KMAX8_RANDQ1.00e+00_CP10.0000_BOTHRK_1_approach_and_separation_velocities_K10_T10');
simulation_output_directory      =('3D_N50016_L70.0_CCR0.85_PHI0.076_EC277.21_DC27.71_KMAX8_RANDQ1.00e+00_CP1.0000_BOTHRK_1_approach_and_separation_velocities');
simulation_output_directory      =sprintf('/scratch.local/data/data-galemd-output/%s',simulation_output_directory);

listing            = dir(strcat((simulation_output_directory),'/avgSeparationVelocity_t*.dat'));
listingName        = {listing(:).name};
listing            = natsortfiles(listingName);

FLOATTYPE          = 'float';
ITER_DATA_GEN      = 100;
DELTA_T            = 0.001;

[fig_0,fig_0_gcf]  = createFig(1,'$\langle \dot{\xi} \rangle$','$\epsilon^{eff}=\frac{\langle \dot{\xi}` \rangle}{ \langle \dot{\xi} \rangle }$','holdon',  'boxon','loglin',14);
 %set(fig_0,'xlim',[0.0 2]);
 %set(fig_0,'ylim',[0.0 1]);


    timestep = (length(listing)-1)*ITER_DATA_GEN;
    time_end = timestep*DELTA_T;

for step=1:length(listing)

    timestep = (step-1)*ITER_DATA_GEN;
    time     = timestep*DELTA_T
    %|get data|------------------------------------------------------------
    foo = char(listing(1,step));
    timestring = (foo(strfind(foo,'_t')+2 :1: strfind(foo,'.dat')-1));
    
    v_approach   = getVector(simulation_output_directory, 'avgApproachVelocity_t', timestring, FLOATTYPE);
    v_separation = getVector(simulation_output_directory, 'avgSeparationVelocity_t', timestring, FLOATTYPE);
    
    v_approach   = v_approach(1,:)./v_approach(2,:);
    v_separation = v_separation(1,:)./v_separation(2,:);
    
    lc=plot(fig_0,-v_separation,-v_approach./v_separation,'.','markersize',5,'color','r');%[1-time/time_end 1-time/time_end 1-time/time_end]
    
end

simulation_output_directory      =('3D_N50016_L70.0_CCR0.85_PHI0.076_EC277.21_DC27.71_1_approach_and_separation_velocities_neutral_T10');
simulation_output_directory       =('3D_N50016_L70.0_CCR0.85_PHI0.076_EC277.21_DC27.71_1_approach_and_separation_velocities_neutral');
simulation_output_directory       =sprintf('/scratch.local/data/data-galemd-output/%s',simulation_output_directory);

listing     = dir(strcat((simulation_output_directory),'/avgSeparationVelocity_t*.dat'));
listingName = {listing(:).name};
listing     = natsortfiles(listingName);


for step=1:10:length(listing)

    timestep = (step-1)*ITER_DATA_GEN;
    time     = timestep*DELTA_T
    %|get data|------------------------------------------------------------
    foo = char(listing(1,step));
    timestring = (foo(strfind(foo,'_t')+2 :1: strfind(foo,'.dat')-1));
    
    v_approach   = getVector(simulation_output_directory, 'avgApproachVelocity_t', timestring, FLOATTYPE);
    v_separation = getVector(simulation_output_directory, 'avgSeparationVelocity_t', timestring, FLOATTYPE);
    
    v_approach   = v_approach(1,:)./v_approach(2,:);
    v_separation = v_separation(1,:)./v_separation(2,:);
    
    ln=plot(fig_0,-v_separation,-v_approach./v_separation,'.','markersize',5,'color','b');%[1-time/time_end 1-time/time_end 1-time/time_end]
    
end

    v_model = 0:1e-4:2;
    e_model = 1-0.45.*v_model.^(1/5);
    lm=plot(fig_0,v_model,e_model,'-k','linewidth',1);
    
    leg = legend(fig_0,[ln,lc,lm],'neutral','charged','viscoelastic model');
    set(leg,'interpreter','latex','box','off','fontsize',14,'location','southwest');     

    txt = text(fig_0,0.01,0.38,'$\epsilon=1-C \langle \dot{\xi} \rangle^{1/5}\pm ...$');
    set(txt,'interpreter','latex','fontsize',14);
  
end

