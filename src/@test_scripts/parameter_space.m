function [] = parameter_space(obj)

close all;
set(0,'DefaultFigureVisible','on');
cd '/scratch.local/data/singh/dev/matbox/out-fractal-aggregation/Nc_2';

phi_s=["0p002", "0p0036", "0p007", "0p013", "0p030", "0p076", "0p137",  "0p213"];
k_s  =["0p001", "0p0036", "0p01",  "0p023", "0p0625", "0p125", "0p4",   "1p0", "5p0"];

phi=[0.002 0.0036 0.007 0.013 0.030 0.076 0.137 0.213];
k  =[0.001 0.0036 0.01 0.023 0.0625 0.125 0.4 1.0 5.0];

[PHI,K]=meshgrid(phi,k);

[fig_1,fig_1_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
[fig_2,fig_2_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
[fig_3,fig_3_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
[fig_4,fig_4_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);

li=0;
l1_handle={};
l1_name={};

for i=5:length(phi_s)
for j=7:length(k_s)

        li=li+1;
        
        outdir = sprintf('phi_%s_K_%s',phi_s(i),k_s(j));    
        if exist(outdir,'dir')
            cd(outdir);
            pwd            
            
            load t;
            color = [rand(1), rand(1), rand(1)];
            
            %|T|-----------------------------------------------------------            
            load T_clusters;
            y = T_clusters;
                        
            l1_handle{li} = plot(fig_1,t,y,'s','color',color,'markersize',8, 'linewidth', 1.2); 
            l1_name{li}   = sprintf('$\\phi=%.3f$',phi(i));
                        
            set(fig_1,'xlim',[10^-1 10^5]);
            set(fig_1,'ylim',[1.5*10^-6 5]);
            set(fig_1,'xscale','log');
            set(fig_1,'yscale','log');
            fig_1.XTick = [0.01 1 100 10000];
            fig_1.YTick = [1e-5 1e-4 1e-3 1e-2 1e-1 1e-0 1e1];
            
            %|<dq^2>|-----------------------------------------------------------
            load var_charge_clusters;
            y = var_charge_clusters;
            
            plot(fig_2,t,y,'s','color',color,'markersize',8, 'linewidth', 1.2); 
            l2_name{li}   = sprintf('$\\phi=%.3f$',phi(i));
            set(fig_2,'xlim',[10^-1 10^5]);
            set(fig_2,'ylim',[2*10^-6 10^-2]);
            set(fig_2,'xscale','log');
            set(fig_2,'yscale','log');
            fig_2.XTick = [1 100 10000];
            fig_2.YTick = [1e-6 1e-5 1e-4 1e-3 1e-2 1e-1 1e-0 1e1];
            
            %|n|-----------------------------------------------------------            
            load no_of_clusters;
            y = no_of_clusters./70^3;

            plot(fig_3,t,y,'s','color',color,'markersize',8, 'linewidth', 1.2); 
            l3_name{li}   = sprintf('$\\phi=%.3f$',phi(i));
            set(fig_3,'xlim',[0.1 10^5]);
            %set(fig_3,'ylim',[1e-4 1e-1]);
            set(fig_3,'xscale','log');
            set(fig_3,'yscale','log'); 
            fig_3.XTick = [1 100 10000];
            
            %|K|-----------------------------------------------------------            
            load K_clusters;
            y = K_clusters;

            plot(fig_4,t,y,'s','color',color,'markersize',8, 'linewidth', 1.2); 
            l4_name{li}   = sprintf('$\\phi=%.3f,\\mathcal{K}=%.3f$',phi(i),k(j));
            set(fig_4,'xlim',[10^-1 10^4]);
            set(fig_4,'ylim',[10^-6 10^2]);
            set(fig_4,'xscale','log');
            set(fig_4,'yscale','log');       
            fig_4.XTick = [1 100 10000];

            cd('..');
        end
        
    end
end




%|legends|------------------


tt = 3:1:3000;
plot(fig_1,tt,0.017.*10^2.*tt.^(-5/3),'-k','linewidth',2);
txt=text(fig_1,7.6,0.00015,'$-5/3$');
set(txt,'interpreter','latex','fontsize',18);


tt = 50:1:4000;
plot(fig_2,tt,0.000175.*10^2.*tt.^(-1),'-k','linewidth',2);
txt=text(fig_2,90,0.000035,'$-1$');
set(txt,'interpreter','latex','fontsize',18);

leg = legend(fig_4,l4_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','northwest');

leg = legend(fig_5,l5_name);    
set(leg,'interpreter','latex','box','off','fontsize',14,'location','southeast');

leg = legend(fig_6,l6_name);    
set(leg,'interpreter','latex','box','off','fontsize',14,'location','southeast');
tt = 100:1:0.8*10^3;
plot(fig_6,tt,10^-2.5.*tt.^(3/2),'-k','linewidth',2);
txt=text(fig_6,1000,0.005,'$3/2$');
set(txt,'interpreter','latex','fontsize',14);

leg = legend(fig_7,l7_name);    
set(leg,'interpreter','latex','box','off','fontsize',14,'location','southeast');

end