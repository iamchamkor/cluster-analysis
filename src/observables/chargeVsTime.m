function [qq] = chargeVsTime(fig_handle,charge,time)

    global N_PARTICLE;

    if(fig_handle~=0)
        %qq = sum((charge - mean(charge)).^2)/N_PARTICLE
        qq = var(charge);
        qm = min(charge);
        plot(fig_handle,time,qq,'or','markersize',10);
        plot(fig_handle,time,qm,'or','markersize',10);
    else
        qq  = 0;
    end
    
end