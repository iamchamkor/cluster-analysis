function [leg_3,Tg_haff,Tg_sim] = granularTemperaturePrecise(fig_handle,time,velocityX,velocityY,velocityZ,positionX,positionY,positionZ)

    global N_PARTICLE;
    global CELL_LENGTH;

if(fig_handle~=0)
    
        nboxes=10;
        
        np_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumX_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumY_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumZ_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        
        fluctuating_velocityX = velocityX - velocityX;
        fluctuating_velocityY = velocityY - velocityY;
        fluctuating_velocityZ = velocityZ - velocityZ;
        
        for(pid=1:N_PARTICLE)
            ibox=floor( (positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes);
                fprintf('problem in binning!')
            else
                np_box(ibox+1,jbox+1,kbox+1) = np_box(ibox+1,jbox+1,kbox+1)+1;
                momentumX_box(ibox+1,jbox+1,kbox+1) = momentumX_box(ibox+1,jbox+1,kbox+1) + 1*velocityX(pid);
                momentumY_box(ibox+1,jbox+1,kbox+1) = momentumY_box(ibox+1,jbox+1,kbox+1) + 1*velocityY(pid);
                momentumZ_box(ibox+1,jbox+1,kbox+1) = momentumZ_box(ibox+1,jbox+1,kbox+1) + 1*velocityZ(pid);
            end
        end
        
%         vol_particle = (4/3)*pi*0.5^3;
%         vol_box      = (CELL_LENGTH/nboxes)^3;

%         com_velocityX_box = momentumX_box./(N_PARTICLE*1);
%         com_velocityY_box = momentumY_box./(N_PARTICLE*1);
%         com_velocityZ_box = momentumZ_box./(N_PARTICLE*1);

        com_velocityX_box = momentumX_box./np_box;
        com_velocityY_box = momentumY_box./np_box;
        com_velocityZ_box = momentumZ_box./np_box;

        com_velocityX_box(isnan(com_velocityX_box))=0;
        com_velocityY_box(isnan(com_velocityY_box))=0;
        com_velocityZ_box(isnan(com_velocityZ_box))=0;
        
        for(pid=1:N_PARTICLE)
            ibox=floor( (positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                fluctuating_velocityX(pid) = velocityX(pid) - com_velocityX_box(ibox+1,jbox+1,kbox+1);
                fluctuating_velocityY(pid) = velocityY(pid) - com_velocityY_box(ibox+1,jbox+1,kbox+1);
                fluctuating_velocityZ(pid) = velocityZ(pid) - com_velocityZ_box(ibox+1,jbox+1,kbox+1);                                
            end
        end
        

    global HAFFS_EXPONENT;
    global TAU_INV;
    global T_HAFF_YSHIFT;

    if(fig_handle~=0)
        v_abs_2    = (velocityX.^2 + velocityY.^2 + velocityZ.^2);
        v_abs_2    = sum(v_abs_2)/N_PARTICLE;
        ke         = (1/2)*1.0*v_abs_2;        
        Tg_sim     = (2/3)*ke;
        Tg_haff    = T_HAFF_YSHIFT*(TAU_INV*time)^(HAFFS_EXPONENT); %+ 3.268*0.01* (TAU_INV*time)^(-11/6);
        gtemp1     = plot(fig_handle,time,Tg_haff,'ok','markerfacecolor','k','markersize',5);
        gtemp2     = plot(fig_handle,time,Tg_sim,'*b');
        
        
        v_abs_2    = (fluctuating_velocityX.^2 + fluctuating_velocityY.^2 + fluctuating_velocityZ.^2);
        v_abs_2    = sum(v_abs_2)/N_PARTICLE;
        ke         = (1/2)*1.0*v_abs_2;        
        Tg_sim     = (2/3)*ke;
        gtemp3     = plot(fig_handle,time,Tg_sim,'ob');
        
        leg_3      = legend(fig_handle,[gtemp1,gtemp2,gtemp3],'Haff','Simulation $\frac{1}{2}m\langle v_i^2 \rangle$','Simulation $\frac{1}{2}m\langle(v_i - V)^2\rangle$');
        set(leg_3,'interpreter','latex','box','off');
%         if(SAVE_TEMPERATURE == 1)
%             Tg_collisional(step) = Tg_sim;
%             t_collisional(step)  = time;
%         end
    end
        
end

end