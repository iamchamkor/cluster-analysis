function [leg_3,Tg_haff,ke] = granularTemperature(fig_handle,time,velocityX,velocityY,velocityZ)

    global N_PARTICLE;
    global HAFFS_EXPONENT;
    global TAU_INV;
    global T_HAFF_YSHIFT;

    if(fig_handle~=0)
        v_abs      = sqrt(velocityX.^2 + velocityY.^2 + velocityZ.^2);
        ke         = (1/2)*1.0*(v_abs).^2;
        ke         = sum(ke)/N_PARTICLE;
        Tg_sim     = (2/3)*ke;
        Tg_haff    = T_HAFF_YSHIFT*(TAU_INV*time)^(HAFFS_EXPONENT); %+ 3.268*0.01* (TAU_INV*time)^(-11/6);
        gtemp1     = plot(fig_handle,time,Tg_haff,'ok','markerfacecolor','k','markersize',5);
        gtemp2     = plot(fig_handle,time,Tg_sim,'ob');
        leg_3      = legend(fig_handle,[gtemp1,gtemp2],'Haff`s law','Simulation');
%         if(SAVE_TEMPERATURE == 1)
%             Tg_collisional(step) = Tg_sim;
%             t_collisional(step)  = time;
%         end
    end
end