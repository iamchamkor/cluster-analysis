function [ke,leg] = kineticEnergy(fig_handle,time,velocityX,velocityY,velocityZ)
    
    global N_PARTICLE;
    global T_HAFF_YSHIFT;
    global TAU_INV;
    global HAFFS_EXPONENT;

    if(fig_handle~=0)
        ke         = sum(0.5.*1.0.*(velocityX.^2 + velocityY.^2 + velocityZ.^2));
        ke         = sum(ke)/N_PARTICLE;
        %Tg_haff    = T_HAFF_YSHIFT*(TAU_INV*time)^(HAFFS_EXPONENT); %+ 3.268*0.01* (TAU_INV*time)^(-11/6);
        %kepp1 = plot(fig_handle,time,Tg_haff,'ok','markerfacecolor','k','markersize',2);
        kepp2 = plot(fig_handle,time,ke,'or');
        leg   = legend(fig_handle,[kepp2],'Simulation');
    else
        leg   = 0;       
    end
    
end