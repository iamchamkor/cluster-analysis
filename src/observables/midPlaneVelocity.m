function [] = midPlaneVelocity(fig_handle,time,density,positionX,positionY,positionZ,velocityX,velocityY,velocityZ)

    global N_PARTICLE;
    global CELL_LENGTH;
    global VIEW_ANGLE;
    
        nboxes=31;      
        
        np_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        density_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumX_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumY_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumZ_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        
        for(pid=1:N_PARTICLE)
            ibox=floor( (positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                np_box(ibox+1,jbox+1,kbox+1)        = np_box(ibox+1,jbox+1,kbox+1)        + 1;
                density_box(ibox+1,jbox+1,kbox+1)   = density_box(ibox+1,jbox+1,kbox+1)   + density(pid);
                momentumX_box(ibox+1,jbox+1,kbox+1) = momentumX_box(ibox+1,jbox+1,kbox+1) + velocityX(pid);
                momentumY_box(ibox+1,jbox+1,kbox+1) = momentumY_box(ibox+1,jbox+1,kbox+1) + velocityY(pid);
                momentumZ_box(ibox+1,jbox+1,kbox+1) = momentumZ_box(ibox+1,jbox+1,kbox+1) + velocityZ(pid);
            end
        end
        
        com_density_box   = density_box  ./np_box;
        com_momentumX_box = momentumX_box./np_box;
        com_momentumY_box = momentumY_box./np_box;
        com_momentumZ_box = momentumZ_box./np_box;      
        
        com_density_box(isnan(com_density_box))    =0;
        com_momentumX_box(isnan(com_momentumX_box))=0;
        com_momentumY_box(isnan(com_momentumY_box))=0;
        com_momentumZ_box(isnan(com_momentumZ_box))=0;        
       
    if(fig_handle~=0)
        
        nboxes_allocated = nboxes+1;
                       
        %|vx along z|
        vx_center = reshape(com_momentumX_box(nboxes_allocated/2,nboxes_allocated/2,1:nboxes),[nboxes,1]);
        dz        = CELL_LENGTH/nboxes;
        z_center  = dz/2 : dz : CELL_LENGTH-dz/2; 
        z_center  = z_center - CELL_LENGTH/2;
        plot(fig_handle,vx_center(1:nboxes),z_center,'o-');
        
        U   = 0.5;
        mu  = 1/16;
        rho = 1.0;
        nu  = mu/rho;
        vx_analytical = U.*(1-erf((pi/2)/(2*sqrt(nu*time))));
        
        plot(fig_handle,vx_analytical,(pi/2),'sr');
        
        legendInfo = ['$t^*$ = ' num2str(time)];
        leg=legend(fig_handle,legendInfo);
        set(leg,'interpreter','latex','box','off');
        
                      
end