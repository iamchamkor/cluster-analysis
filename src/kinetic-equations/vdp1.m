function dydt = vdp1(t,y)

n  = y(1);
T  = y(2); 
QQ = y(3);

no_of_particles = n * 70^3;

m   = 50016/no_of_particles;

% m = 1/y(3);
% m = (M+y(3))/2;
% M = m;

rho = 6/pi;

d = (6*m/(rho*pi))^(1/3);

 ke = 1;
 %d  = 1;
 %m  = 1;
 s  = 2*QQ;
 
 R  = ke*QQ/(d*T);
 
 %r  = 1.05;

 l1 = sqrt(1-R^2);
 l = sqrt(4-R^2);

 K1 = 5;%1.15344*A*kappa^(2/5);

 %%%%%%%%%%%%%%%%%
 
 C_T_res = n^2 * (10 * 2^(1/5) * d^2 * K1 * T^(8/5) )/( 21*sqrt(pi)*m^(3/5) );

 C_T_agg = n^2 * ( d^2 * T * T^(1/2) )/( 2 * sqrt(pi*m) );

 %%%%%%%%%%%%%%%%%
 
%  a_T_1 = R*(8-5*R^2)*l + pi*(16-10*R^2+3*R^4);
%  a_T_2 = (-32+20*R^2-6*R^4);
%  a_T_3 = -l*(2*pi*(-4+R^2)^2 + R*(-32+28*R^2 + R^4) );
%  a_T_4 = 4*(32-20*R^2+9*R^4);
%  a_T_5 = -R*l*(-32+28*R^2 +R^4) -2*pi*(R^2 * (20-8*l) + R^4 * (-9+l) + 16*(-2+l) );
%  a_T_6 = -4*(32-20*R^2 +9*R^4);
%  
%  dEdt_res  = - C_T_res * (8/l^5) * ( a_T_1 + a_T_2 * atan(R/l) );
% 
%  dEdt_agg  = + C_T_agg * (1/(4*l^5)) * ( ( a_T_3 + a_T_4 * acot(R/l) ) ...
%                                             + ( a_T_5 + a_T_6*atan(R/l) ) ...
%                                         );
 
 dEdt_res  = - C_T_res * (8/l^5) * ( R*(8-5*R^2)*l + pi*(16-10*R^2+3*R^4) + (-32+20*R^2-6*R^4)*atan(R/l) );

 dEdt_agg  = + C_T_agg * (1/(4*l^5)) * ( ( -l*(2*pi*(-4+R^2)^2 + R*(-32+28*R^2 + R^4) ) + 4*(32-20*R^2+9*R^4)*acot(R/l) ) ...
                                            + (-R*l*(-32+28*R^2 +R^4) -2*pi*(R^2 * (20-8*l) + R^4 * (-9+l) + 16*(-2+l) ) -4*(32-20*R^2 +9*R^4)*atan(R/l) ) ...
                                        );
                                    
                                 
                                   
 dEdt = dEdt_res + dEdt_agg;


% C_Q = pi*d*d*2*pi^(-1.5)*sqrt(T)*s^2*((r^2)-1)*2*pi/(4*sqrt(m)*pi*s);%r-formulation
% 
% dqqdt = 0*C_Q * ( 4/(4-R^2)^(5/2) ) * (R * sqrt(4-R^2) * (2+R^2) - 8*(-1+R^2)*acot(R/sqrt(4-R^2)) );%r-formulation

 K = 0.1;
 
 A = (4*T/m)^(1.3) *K^2*(2 + (4/5));

 %%%%%%%%%%%%%%
 
 C_Q_res = n^2 * (25/252)*sqrt(pi)*d^2;%kinetic energy dependent formulation
 
 C_Q_agg = n^2 * s * (2 * d^2 * T^(1/2) / sqrt(pi*m) );

 
 
 dqqdt_res = -(C_Q_res/pi) * ( 8*A/l^5 ) * (2*R*l*(-1+R^2)-pi*(4-2*R^2+R^4)+2*(4-2*R^2+R^4)*atan(R/l));%kinetic energy dependent formulation
 
 dqqdt_agg = -C_Q_agg * ( -(( sqrt(pi)*R*(l*(pi*(-4+R^2)*(1+2*R^2)+2*R*l1*(-8-54*R^2+33*R^4+2*R^6))-2*(l^5)*(1+2*R^2)*atan(R/l1)-16*(l1^5)*(4+5*R^2)*atan(l/R)))/(16*l*sqrt(pi-pi*R^2)*(4-5*R^2+R^4)^2))...
                          +(sqrt(pi)*R*(-(l^5)*(1+2*R^2)*atan(l1/R)+l1*(R*l*(8+54*R^2-33*R^4-2*R^6)+8*(-1+R^2)^2 * (4+5*R^2) * atan(l/R) )))/(8*l*sqrt(pi-pi*R^2)*(4-5*R^2+R^4)^2)... 
                        );
 
 dqqdt = dqqdt_res + dqqdt_agg;
 

 C_T_m =    n^2 * ( 2 * d^2 * T^(1/2) / sqrt(pi*m) );

 dNdt_res  = 0;
 
 dNdt_agg  = C_T_m * ( 1 / ( 4 * l1^3 * l^3 )) * ...
                (-2 * R^2 * l^3 * atan(l1/R) + l1 * ( (R*l*(-4+7*R^2)+pi*(-1+R^2)*(8-4*l+R^2 *(-6+l)) + 4*(4-7*R^2+3*R^4)*atan(R/l)) + (l*(R*(-4+7*R^2)+pi*(4-5*R^2+R^4))-4*(4-7*R^2 +3*R^4)*atan(l/R)) ) );
            
            
 dNdt = dNdt_res + 30.0*dNdt_agg;    
 
 dydt = [ dNdt; dEdt; dqqdt ];

end