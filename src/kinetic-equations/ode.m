%clear all
close all
clc

output_dir           =('/scratch.local/data/singh/dev/matbox');
[status, msg, msgID] = mkdir(output_dir);

[fig_1,fig_1_gcf]  = createFig(1,         'time $t v_\textrm{ref}/d$',         'granular temperature $T$', 'holdon',   'boxon','loglog',14);
[fig_2,fig_2_gcf]  = createFig(1,         'time $t v_\textrm{ref}/d$',         'charge variance $\langle\delta q^2\rangle$', 'holdon',   'boxon','loglog',14);
[fig_3,fig_3_gcf]  = createFig(1,         'time $t v_\textrm{ref}/d$',         'concentration $n$', 'holdon',   'boxon','loglog',20);
[fig_4,fig_4_gcf]  = createFig(1,         'time $t v_\textrm{ref}/d$',         '$\frac{k_e \langle\delta q^2\rangle}{T d}$', 'holdon',   'boxon','loglog',14);

[t,y] = ode45(@vdp1,[0 2*10^3],[50016/70^3; 1; 1e-16]);

n = y(:,1);
T = y(:,2);
QQ= y(:,3);

%temperature
Th1 = 1.*t.^(-5/3);
plot(fig_1,t,T,'-'); 
plot(fig_1,t,Th1,'-'); 

%charge-fluctuations
Th = 0.001.*t.^(0.5);
plot(fig_2,t,QQ,'-o'); 


t_theory   = t;
T_theory   = T;
dqq_theory = QQ;
n_theory   = n;


no_of_particles = n_theory .* 70^3;
m_theory        = 50016./no_of_particles;
rho             = 6/pi;
d_theory        = (6.*m_theory./(rho*pi)).^(1/3);

%concentration
Th = t.^(0.1);
plot(fig_3,t,n_theory,'-b','linewidth',2,'markersize',10);
%Th1 = 1.*t.^(1.5);
%plot(fig_3,t,Th1,'-'); 

%R
R_theory = 1.*dqq_theory./(T_theory.*d_theory);
plot(fig_4,t,R_theory,'-o');

save(sprintf('%s/t_theory.mat',output_dir), 't_theory');
save(sprintf('%s/T_theory.mat',output_dir), 'T_theory');
save(sprintf('%s/dqq_theory.mat',output_dir), 'dqq_theory');
save(sprintf('%s/n_theory.mat',output_dir), 'n_theory');
save(sprintf('%s/R_theory.mat',output_dir), 'R_theory');