function [s] = domain(fig_handle,positionX,positionY,positionZ,color_condition,color,axis_condition,time)

    VIEW_ANGLE  = 30;
    CELL_LENGTH = 70;
    PARTICLE_SKIP = 1;
    DOT_SIZE = 5;

    if(fig_handle~=0)
        %s = scatter3(fig_handle,positionX(1:PARTICLE_SKIP:end),positionY(1:PARTICLE_SKIP:end),...
        %    positionZ(1:PARTICLE_SKIP:end),DOT_SIZE,...
        %    charge(1:PARTICLE_SKIP:end),'filled');

        if(strcmp(color_condition,'fixed_color'))
        s = scatter3(fig_handle,positionX(1:PARTICLE_SKIP:end),positionY(1:PARTICLE_SKIP:end),...
            positionZ(1:PARTICLE_SKIP:end),DOT_SIZE,...
            color,'filled');
        
        elseif(strcmp(color_condition,'random_color'))
        s = scatter3(fig_handle,positionX(1:PARTICLE_SKIP:end),positionY(1:PARTICLE_SKIP:end),...
            positionZ(1:PARTICLE_SKIP:end),DOT_SIZE,...
            'filled');
        else
        printf('particles in box: problem in color!');
        end

        set(fig_handle,'XTick',[]);
        set(fig_handle,'YTick',[]);
        set(fig_handle,'ZTick',[]);
        
        if(~strcmp(axis_condition,'show_axis_lines'))
        fig_handle.XAxis.Color='w';
        fig_handle.YAxis.Color='w';
        fig_handle.ZAxis.Color='w';
        end
        
%         if(CHARGE==1)
%             %cbar = colorbar(fig_handle);
%             %colormap(fig_handle,brewermap([],'PuBuGn'));
%             %colormap(cbar,brewermap([],'PuBuGn'));
%             %ylabel(cbar,'$q$','fontsize',FONT_SIZE,'interpreter','latex');
%             %set(cbar,'fontsize',FONT_SIZE,'fontweight','normal','FontName','times');
%         end
        box(fig_handle,'on'); 
        
        if(~strcmp(axis_condition,'show_axis_lines'))
            fig_handle.BoxStyle = 'back';
        else
            fig_handle.BoxStyle = 'full';
        end
        
        set(fig_handle,'View',[-VIEW_ANGLE,VIEW_ANGLE]);
        axis(fig_handle,[-CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2]);
                
        
        txt=text(fig_handle,-70,-35,-18,sprintf('$t^*$=%d',time));
        set(txt,'interpreter','latex','fontsize',20,'fontweight','normal','FontName','times');
        
    else
        s   = 0;
    end
    
end