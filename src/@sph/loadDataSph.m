function [obj] = loadDataSph(obj,step)

    foo        = char(obj.listing(1,step));
    timestring = (foo(strfind(foo,'_t')+2 :1: strfind(foo,'.dat')-1));

    %|particle-position|-------------------------------------------------------------
    obj.positionX = getVector(obj.sim_directory, 'positionX_fluid_particle_t', timestring, obj.FLOATTYPE);
    obj.positionX = obj.CELL_LENGTH*obj.positionX;
    obj.positionY = getVector(obj.sim_directory, 'positionY_fluid_particle_t', timestring, obj.FLOATTYPE);
    obj.positionY = obj.CELL_LENGTH*obj.positionY;
    obj.positionZ = getVector(obj.sim_directory, 'positionZ_fluid_particle_t', timestring, obj.FLOATTYPE);
    obj.positionZ = obj.CELL_LENGTH*obj.positionZ;

    %|particle-velocity|-------------------------------------------------------------
    obj.velocityX = getVector(obj.sim_directory, 'velocityX_fluid_particle_t', timestring, obj.FLOATTYPE);
    obj.velocityY = getVector(obj.sim_directory, 'velocityY_fluid_particle_t', timestring, obj.FLOATTYPE);
    obj.velocityZ = getVector(obj.sim_directory, 'velocityZ_fluid_particle_t', timestring, obj.FLOATTYPE);

    %|particle-energy|---------------------------------------------------------------
    if(obj.energy_switch == 1)
        obj.energy = getVector(obj.sim_directory, 'energy_fluid_particle_t',   timestring, obj.FLOATTYPE);
    end    
    
    %|particle-density|--------------------------------------------------------------
    if(obj.density_switch == 1)
        obj.density = getVector(obj.sim_directory, 'density_fluid_particle_t', timestring, obj.FLOATTYPE);
    end
    
    %|particle-charge|---------------------------------------------------------------
    if(obj.charge_switch == 1)
        obj.charge = getVector(obj.sim_directory, 'charge_fluid_particle_t', timestring, obj.FLOATTYPE);
    end
    
    %|particle-force|----------------------------------------------------------------
    if(obj.force_switch == 1)
        obj.forceX = getVector(obj.sim_directory, 'forceX_fluid_particle_t', timestring, obj.FLOATTYPE);
        obj.forceY = getVector(obj.sim_directory, 'forceY_fluid_particle_t', timestring, obj.FLOATTYPE);
        obj.forceZ = getVector(obj.sim_directory, 'forceZ_fluid_particle_t', timestring, obj.FLOATTYPE);
    end
    
end