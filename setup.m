%|paths|-------------------------------------------------------------------
matbox_path = '/scratch.local/data/singh/dev/matbox'; % matbox mount point

addpath(matbox_path);

%|ext|---------------------------------------------------------------------
addpath(sprintf('%s/ext/natsortfiles/',matbox_path));
addpath(sprintf('%s/ext/brewermap/',matbox_path));


%|src/submodules/tools|----------------------------------------------------
addpath(sprintf('%s/src/',matbox_path));
addpath(sprintf('%s/src/cluster-analysis',matbox_path));
addpath(sprintf('%s/src/langevin-equation/',matbox_path));
addpath(sprintf('%s/src/kinetic-equations/',matbox_path));
addpath(sprintf('%s/src/sph/',matbox_path));
addpath(sprintf('%s/src/observables/',matbox_path));
addpath(sprintf('%s/src/visualization/',matbox_path));
addpath(sprintf('%s/src/common/',matbox_path));
addpath(sprintf('%s/src/statistics/',matbox_path));
addpath(sprintf('%s/src/test_scripts/',matbox_path));

%|other setups|------------------------------------------------------------
set(0,'RecursionLimit',5000)