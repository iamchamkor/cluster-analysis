clc;
clear all;
close all;

setup;
set(0,'DefaultFigureVisible','on');

obj_sph         = sph(matbox_path); 
obj_observables = observables;
obj_cluster     = cluster;
obj_store       = store(obj_sph);

obj_sph.initMovie(0);

obj_sph.END_STEP

istore = 1;
%for iter=1:obj_sph.TIME_STEP_SKIP:obj_sph.END_STEP
for iter=1:obj_sph.END_STEP
    
    if(obj_sph.TIME_BASE==1)
       step = iter; 
    else
       step = floor(obj_sph.TIME_BASE^iter)+1;    
    end
    
    pause(1e-16);
    
    timestep = (step-1)*obj_sph.ITER_DATA_GEN;
    time     = timestep*obj_sph.DELTA_T
    istore;              
    
    obj_sph = obj_sph.loadDataSph(step);
    
    obj_observables.observablesMain(obj_sph, time, iter);
          
    %obj_cluster.clusterMain(obj_sph, step, time);        
    
    %istore=obj_store.store2memory(obj_cluster, obj_observables, time, istore);

    if (obj_sph.MOVIE)
        frame = getframe(obj_observables.fig_1_gcf);          
        writeVideo(obj_sph.video_name, frame);
    end  
    
end

if (obj_sph.MOVIE)
    close(obj_sph.video_name);
end

if(obj_observables.PLOT_DISSIPATION)
    dt = obj_sph.DELTA_T*obj_sph.ITER_DATA_GEN;
    e  = -gradient(obj_observables.ke_sph, dt);
    plot(obj_observables.fig_41,obj_observables.t_sph,e,'-b','linewidth',2);
    %set(obj_observables.fig_41,'ylim',[0 0.016]);
end

pause(1);
obj_store.store2files();
pause(1);
%|end|