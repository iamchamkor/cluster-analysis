close all; clear all; clc
PLOT_NEUTRAL_CASE = 1;
PLOT_CHARGED_CASE = 1;
[fig_0,fig_0_gcf]  = createFig(1,'$k$','$\lambda_i$','holdoff','boxon','linlin',22);
xlim([0 0.3]);
ylim([-0.2 0.11]);
syms Z M K E k
e = 0.7;
a2 = 16*(1-e)*(1-2*e^2)/(9+24*3+(8*3-41)*e+30*e^2*(1-e));
nu1= (3-3*e+2*3)*(1+e)*(1-a2/32)/(4*3);
nu2= (1+e)*( (3-1)/2+(3*(3+8)*(1-e))/16+(4+5*3-3*(4-3)*e)*a2/512 )/(3-1);
Z  = (2+3)*(1-e^2)*(1+3*a2/16)/(4*3);
E  = 1/(nu1-0.5*Z);
K  = (1+2*a2)/(nu2-3*Z);
M  = 2*Z*(K+(2/3)*a2/Z)/((4/3)*nu2-3*Z);
if(PLOT_NEUTRAL_CASE)
MAT = [...
    0,                      0,                      -i*k; ...
    -0.5*Z-(5/2)*M*k^2,     -0.25*Z-(5/2)*K*k^2,    -(2/3)*i*k; ...
    -0.5*i*k,               -0.5*i*k,               0.25*Z-(4/3)*E*k^2;...
    ];
LAMBDA = real(eig(MAT));
hold on;
fplot(k,LAMBDA(1),[0 0.3],'-k','linewidth',1.5);
fplot(k,LAMBDA(2),[0 0.3],'-b','linewidth',1.5);
fplot(k,LAMBDA(3),[0 0.3],'-','linewidth',1.0,'color','r');
end
P=1;
R=0.9000/P;
MAT = [...
  0,                  0,                  -i*k,                0; ...
  -0.5*Z-(5/2)*M*k^2,-0.25*Z-(5/2)*K*k^2, -(2/3)*i*k,          0; ...
  -0.5*i*k,          -0.5*i*k,            0.25*Z-(4/3)*E*k^2,  R*i*k/(i*k)^2;...  
   0,                 0,                  -i*k,                0;...               
    ];
LAMBDA = real(eig(MAT));
hold on;
fplot(k,LAMBDA(1),'--','color', [.8 .8 .8],'linewidth',0.5);
if(PLOT_CHARGED_CASE)
fplot(k,LAMBDA(2),[0 0.3],'--k','linewidth',1.5);
fplot(k,LAMBDA(3),[0 0.3],'--b','linewidth',1.5);
fplot(k,LAMBDA(4),[0 0.3],'--r','linewidth',1.0);
txt2 = sprintf('$\\epsilon=%.1f, \\mathcal{R}=%.4f$', e, R);
leg2=text(fig_0, 0.13, 0.085, txt2);
set(leg2, 'interpreter', 'latex', 'fontsize', 22);          
end
if(0)
    txt3 = sprintf('$\\lambda_1$');
    leg3 = text(fig_0, 0.05, 0.05, txt3);
    set(leg3, 'interpreter', 'latex', 'fontsize', 18);
    
    txt4 = sprintf('$\\lambda_2$');
    leg4 = text(fig_0, 0.01, 0.006, txt4);
    set(leg4, 'interpreter', 'latex', 'fontsize', 18);
    
    txt5 = sprintf('$\\lambda_3$');
    leg5 = text(fig_0, 0.01, -0.065, txt5);
    set(leg5, 'interpreter', 'latex', 'fontsize', 18);    
end



