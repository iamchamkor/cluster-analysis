clear all;
close all;
clc;
syms b qi qk ke pi L m rik ri rk
Uri_real = ke*qk*erfc(b*abs(rk-ri))/abs(rk-ri)
Fi_real  = -qi*gradient(Uri_real,ri)
Uri_reciprocal = (ke*4*pi/L^3)*qk * (1/abs(m)^2) * exp(-abs(m)^2 / (4*b^2))*cos(m * (rk-ri))
Fi_reciprocal  = -qi*gradient(Uri_reciprocal,ri)