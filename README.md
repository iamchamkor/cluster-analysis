This cluster analysis project started during the following research:

[1. Electrification in granular gases leads to constrained fractal growth](https://doi.org/10.1038/s41598-019-45447-x), Scientific Reports, 9:9049, 2019.

[2. Aggregation and pattern formation in charged granular gases](https://ediss.uni-goettingen.de/handle/21.11130/00-1735-0000-0003-C1B2-9), PhD thesis, University of Goettingen, 2019.


Full description and working examples will be added here shortly. To use the program, please include the following in your copy.

    matbox
    Copyright (C) 2021  Chamkor Singh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
